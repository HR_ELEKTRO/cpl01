#include <stdio.h>

/* © 2015 Harry Broeders */
/* benader cos(x) met de volgende formule cos(x) = 1 - x^2/2! + x^4/4! - x^6/6! + ... */
/* in deze formule betekent x^4: x tot de macht 4. Dat werkt niet in C. */
/* in deze formule betekent 4!: 4 faculteit. Dat werkt niet in C. */
/* stap 0; formule herschrijven cos(x) = x^0/0! - x^2/2! + x^4/4! - x^6/6! + ... */
/* waarbij x de hoek in radialen is */
/* aantal termen is een variabele die moet worden ingelezen */

int main(void)
{
    /* stap 1: lees x (floating point getal waarvan de cos bepaald moet worden) */
    /* stap 2: lees aantal termen in (geheel getal > 0) */
    /* stap 3: bepaal nummer voor elke term: 0, 1, 2, 3, ..., aantalTermen-1 */
    /* stap 4: bepaal constante voor elke term: 0, 2, 4, 6, ... */
    /* stap 5: bepaal teller van elke term: x^0, x^2, x^4, x^6 */

    double x;
    int aantalTermen;

    printf("Geef x: ");
    scanf("%lf", &x);

    do
    {
        printf("Geef het aantal termen: ");
        scanf("%d", &aantalTermen);
    }
    while (aantalTermen <= 0);

    double termTeller = 1;
    for (int termNummer = 0; termNummer < aantalTermen; termNummer = termNummer + 1)
    {
        int termConstante = termNummer * 2;
        printf("Test: termTeller = %f\n", termTeller);
        /* bereken volgende termTeller */
        termTeller = termTeller * x * x;
    }

    printf("cos(%f) benaderd met %d termen = %f\n", x, aantalTermen, 0.0); /* dit is nog niet het goede antwoord! */

    return 0;
}
