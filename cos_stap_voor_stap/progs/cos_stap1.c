#include <stdio.h>

/* © 2015 Harry Broeders */
/* benader cos(x) met de volgende formule cos(x) = 1 - x^2/2! + x^4/4! - x^6/6! + ... */
/* in deze formule betekent x^4: x tot de macht 4. Dat werkt niet in C. */
/* in deze formule betekent 4!: 4 faculteit. Dat werkt niet in C. */
/* stap 0; formule herschrijven cos(x) = x^0/0! - x^2/2! + x^4/4! - x^6/6! + ... */
/* waarbij x de hoek in radialen is */
/* aantal termen is een variabele die moet worden ingelezen */

int main(void)
{
    /* stap 1: lees x (floating point getal waarvan de cos bepaald moet worden) */

    double x;

    printf("Geef x: ");
    scanf("%lf", &x);

    printf("cos(%f) = %f\n", x, 0.0); /* dit is nog niet het goede antwoord! */

    return 0;
}
