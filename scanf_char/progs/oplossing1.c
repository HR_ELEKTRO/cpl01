#include <stdio.h>

int main(void)
{
    printf("Geef een getal: ");
    int getal;
    char antwoord;
    do
    {
        scanf("%d", &getal);
        printf("Het ingevoerde getal is: %d\n", getal);
        printf("Is dit correct [J/N]: ");
        scanf("\n%c", &antwoord);
        if (antwoord == 'N')
        {
            printf("Geef dan nu het goede getal: ");
        }
    }
    while (antwoord != 'J');
    printf("Het definitief ingevoerde getal is %d\n", getal);
    return 0;
}
