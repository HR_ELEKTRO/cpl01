#include <stdio.h>

int main(void)
{
    int aantalKoffie = 3;
    int aantalThee = 3;
    int keuze;

    do
    {
        printf("Koffie: ");
        for (int i = 0; i < aantalKoffie; i = i + 1)
        {
            printf("=");
        }
        printf("\nThee:   ");
        for (int i = 0; i < aantalThee; i = i + 1)
        {
            printf("=");
        }

        printf("\nKies Koffie (1) of Thee (2): ");
        scanf("%d", &keuze);

        if (keuze == 1 && aantalKoffie > 0) {
            aantalKoffie = aantalKoffie - 1;
        }

        if (keuze == 2 && aantalThee > 0) {
            aantalThee = aantalThee - 1;
        }
    }
    while (aantalKoffie != 0 || aantalThee != 0);
    return 0;
}
