#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

int telLettersCijfers(char str[], int *p_letters, int *p_cijfers)
{
    int letters = 0;
    int cijfers = 0;
    int lengte = strlen(str);

    for(int i = 0; i < lengte; i++)
    {
        if(isalpha(str[i]))
        {
            letters++;
        }
        if(isdigit(str[i]))
        {
            cijfers++;
        }
    }

    *p_letters = letters;
    *p_cijfers = cijfers;

    return lengte;
}

int main()
{
    char *s = "CPL01 is de *leukste* module van Elektrotechniek!!!1!! :)";
    int cijfers;
    int letters;

    int lengte = telLettersCijfers(s, &letters, &cijfers);

    printf("De string \"%s\" bevat %d letters, %d cijfers en heeft lengte %d.\n", s, letters, cijfers, lengte);

    return 0;
}
