#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int controleerGebruikersnaam(char naam[], char teControlerenNaam[]);
int controleerWachtwoord(int ww, int teControlerenWw);

int controleerGebruikersnaam(char naam[], char teControlerenNaam[])
{
    return strcmp(naam, teControlerenNaam) == 0;
}

int controleerWachtwoord(int ww, int teControlerenWw)
{
    return ww == teControlerenWw;
}

int main()
{
    char gebruikersnaam[10];
    int wachtwoord;
    char naamVergelijk[10] = "student";
    int wwVergelijk = 12345;

    int aantalPogingen = 0;

    do
    {
        fflush(stdin);
        printf("Voer gebruikersnaam in:\n");
        scanf("%9s", gebruikersnaam);
        fflush(stdin);
        printf("Voer wachtwoord in:\n");
        scanf("%d", &wachtwoord);

        if (controleerGebruikersnaam(gebruikersnaam, naamVergelijk) == 1 && controleerWachtwoord(wachtwoord, wwVergelijk)==1)
        {
            aantalPogingen = 0;
        }
        else
        {
            aantalPogingen = aantalPogingen + 1;
        }
    }
    while (aantalPogingen > 0 && aantalPogingen < 3);

    if (aantalPogingen == 3)
    {
        printf("3x verkeerd ingelogd.\n");
    }
    else
    {
        printf("Ingelogd!");
    }

    return 0;
}
