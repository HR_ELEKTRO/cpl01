#include <stdio.h>

int main(void)
{
    int aantalWater = 3;
    int aantalFrisdrank = 3;
    int keuze;

    do
    {
        printf("Water:     ");
        for (int i = 0; i < aantalWater; i = i + 1)
        {
            printf("#");
        }
        printf("\nFrisdrank: ");
        for (int i = 0; i < aantalFrisdrank; i = i + 1)
        {
            printf("#");
        }
        printf("\nWater:     %7.3f%%", aantalWater / 0.03);
        printf("\nFrisdrank: %7.3f%%", aantalFrisdrank / 0.03);
        
        printf("\nKies Water (1) of Frisdrank (2): ");
        scanf("%d", &keuze);

        if (keuze == 1 && aantalWater > 0) {
            aantalWater = aantalWater - 1;
        }

        if (keuze == 2 && aantalFrisdrank > 0) {
            aantalFrisdrank = aantalFrisdrank - 1;
        }
    }
    while (aantalWater != 0 || aantalFrisdrank != 0);
    return 0;
}
