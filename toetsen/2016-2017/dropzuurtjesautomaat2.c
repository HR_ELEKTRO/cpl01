#include <stdio.h>

int main(void)
{
    int aantalDrop = 3;
    int aantalZuurtjes = 3;
    int keuze;

    do
    {
        printf("Drop:     ");
        for (int i = 0; i < aantalDrop; i = i + 1)
        {
            printf("+");
        }
        printf("\nZuurtjes: ");
        for (int i = 0; i < aantalZuurtjes; i = i + 1)
        {
            printf("+");
        }
        printf("\nDrop:     %5.1f%%", aantalDrop / 0.03);
        printf("\nZuurtjes: %5.1f%%", aantalZuurtjes / 0.03);
        printf("\nKies Drop (1) of Zuurtjes (2): ");
        scanf("%d", &keuze);

        if (keuze == 1 && aantalDrop > 0) {
            aantalDrop = aantalDrop - 1;
        }

        if (keuze == 2 && aantalZuurtjes > 0) {
            aantalZuurtjes = aantalZuurtjes - 1;
        }
    }
    while (aantalDrop != 0 || aantalZuurtjes != 0);
    return 0;
}
