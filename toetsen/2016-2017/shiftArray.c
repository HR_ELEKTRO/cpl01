#include <stdio.h>
#include <stdlib.h>

void printArray(int a[], unsigned int n)
{
    for(unsigned int i = 0; i < n-1; i++)
    {
        printf("%3d, ", a[i]);
    }
    printf("%3d\n", a[n-1]);
}

void shiftArray(int src[], int dst[], unsigned int n, unsigned int shift)
{
    for(unsigned int i = 0; i < n; i++)
    {
        dst[(i+shift)%n] = src[i];
    }
}

int main()
{
    int a[14];
    int b[14];

    for(int i = 0; i < 14; i++)
    {
      a[i] = i*i;
    }

    printArray(a, 14);
    shiftArray(a, b, 14, 2);
    printArray(b, 14);
    shiftArray(b, a, 14, 3);
    printArray(a, 14);
    shiftArray(a, b, 8, 1);
    printArray(b, 14);
    printArray(b, 8);

    return 0;
}
