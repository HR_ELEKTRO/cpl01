#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void cartesian2polar(double a, double b, double *r, double *angle)
{
    *r = sqrt(a*a + b*b);
    *angle = atan(b/a);
}

int main()
{
    double a = 2;
    double b = 3;
    double r;
    double angle;

    cartesian2polar(a, b, &r, &angle);

    printf("r     = %f\nAngle = %f\n", r, angle);

    return 0;
}

