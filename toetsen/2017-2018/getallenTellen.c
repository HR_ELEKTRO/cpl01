#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>

// Kies oplossing 1 of 2
#define OPLOSSING 2

#if OPLOSSING == 1
int telGeheleGetallen(char s[])
{
    bool inGetal = false;
    int aantalGeheleGetallen = 0;
    for (size_t i = 0; s[i] != '\0'; i++)
    {
        if (isdigit(s[i]))
        {
            inGetal = true;
        }
        else
        {
            if (inGetal)
            {
                aantalGeheleGetallen++;
                inGetal = false;
            }
        }
    }
    if (inGetal)
    {
        aantalGeheleGetallen++;
    }
    return aantalGeheleGetallen;
}
#elif OPLOSSING == 2
int telGeheleGetallen(char s[])
{
    int aantalGeheleGetallen = 0;
    for (size_t i = 0; s[i] != '\0'; i++)
    {
        if (isdigit(s[i]))
        {
            while (isdigit(s[i]) && s[i+1] != '\0')
            {
                i++;
            }
            aantalGeheleGetallen++;
        }
    }
    return aantalGeheleGetallen;
}
#else
    #error "Ongeldige waarde voor OPLOSSING"
#endif

int main()
{
    printf("%d\n", telGeheleGetallen("Na 3 pogingen heb ik in 2017 uiteindelijk een 10 gehaald voor CPL01"));
    return 0;
}
