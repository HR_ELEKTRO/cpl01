#include <stdio.h>
#include <math.h>

void polar2cartesian(double r, double angle, double* a, double* b)
{
    *a = r * cos(angle);
    *b = r * sin(angle);
}

int main()
{
    double r = 0.5;
    double angle = 3.14159265358979323846;
    double a;
    double b;

    polar2cartesian(r, angle, &a, &b);

    printf("a = %f\nb = %f\n", a, b);

    return 0;
}
