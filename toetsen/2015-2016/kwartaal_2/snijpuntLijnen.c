#include <stdio.h>

int snijpuntLijnen(double a1, double b1, double a2, double b2, double *x, double *y)
{
    if (a1 == a2)
    {
        if (b1 == b2)
        {
            return 2;
        }
        else
        {
            return 0;
        }
    }
    *x = (b2 - b1) / (a1 - a2);
    *y = (a1 * *x) + b1;
    return 1;
}

double scanDouble(char* text)
{
    double d;
    do
    {
        printf("%s: ", text);
        fflush(stdin);
    }
    while (scanf("%lf", &d) != 1);
    return d;
}

int main(void)
{
    double a1 = scanDouble("Geef a1");
    double b1 = scanDouble("Geef b1");
    double a2 = scanDouble("Geef a2");
    double b2 = scanDouble("Geef b2");

    double x, y;
    int res = snijpuntLijnen(a1, b1, a2, b2, &x, &y);
    if (res == 1)
    {
        printf("Snijpunt van lijnen y = %.2f x + %.2f en y = %.2f x + %.2f is (%.2f, %.2f)", a1, b1, a2, b2, x, y);
    }
    else
    {
        if (res == 0)
        {
            printf("De lijnen y = %.2f x + %.2f en y = %.2f x + %.2f lopen parallel", a1, b1, a2, b2);
        }
        else
        {
            printf("De lijnen y = %.2f x + %.2f en y = %.2f x + %.2f zijn gelijk", a1, b1, a2, b2);
        }
    }

    return 0;
}
