#include <stdio.h>

int main(void)
{
    char letter;
    do
    {
        printf("Type een aantal gehele getallen afgesloten door een 0:\n");
        int getal, aantalPos = 0, somPos = 0;
        do
        {
            scanf("%d", &getal);
            if (getal > 0)
            {
                somPos = somPos + getal;
                aantalPos = aantalPos + 1;
            }
        } while (getal != 0);
        double gemiddeldePos = 0;
        if (aantalPos != 0)
        {
            gemiddeldePos = (double)somPos / aantalPos;
        }
        printf("Het gemiddelde van alle positieve getallen is: %0.2f\n", gemiddeldePos);
        printf("Programma herhalen? ");
        fflush(stdin);
        scanf("%c", &letter);
        while (letter != 'j' && letter != 'J' && letter != 'n' && letter != 'N')
        {
            printf("Type j, J, n of N: ");
            fflush(stdin);
            scanf("%c", &letter);
        }
    } while (letter == 'j' || letter == 'J');
    return 0;
}
