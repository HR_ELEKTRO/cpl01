#include <stdio.h>

int main(void)
{
    char letter;
    do
    {
        printf("Type een aantal gehele getallen afgesloten door een 0:\n");
        int getal, aantalNeg = 0, somPos = 0;
        do
        {
            scanf("%d", &getal);
            if (getal > 0)
            {
                somPos = somPos + getal;
            }
            else
            {
                if (getal < 0)
                {
                    aantalNeg = aantalNeg + 1;

                }
            }
        } while (getal != 0);
        printf("De som van alle positieve getallen is: %d\n", somPos);
        printf("Het aantal negatieve getallen is: %d\n", aantalNeg);
        printf("Programma herhalen of stoppen? ");
        fflush(stdin);
        scanf("%c", &letter);
        while (letter != 'h' && letter != 'H' && letter != 's' && letter != 'S')
        {
            printf("Type h, H, s of S: ");
            fflush(stdin);
            scanf("%c", &letter);
        }
    } while (letter == 'h' || letter == 'H');
    return 0;
}
