#include <stdio.h>
#include <math.h>

#define PI 3.14159265358979323846

int angleA(double a, double b, double c, double *A)
{
    double cosA = (b * b + c * c - a * a) / (2 * b * c);
    if (cosA >= -1 && cosA <= 1)
    {
        *A = acos(cosA);
        return 1;
    }
    return 0;
}

double scanPosDouble(char* text)
{
    double d;
    do
    {
        printf("%s: ", text);
        fflush(stdin);
    }
    while (scanf("%lf", &d) != 1 || d < 0);
    return d;
}

int main(void)
{
    double a = scanPosDouble("Geef lengte zijde a");
    double b = scanPosDouble("Geef lengte zijde b");
    double c = scanPosDouble("Geef lengte zijde c");

    double A;
    if (angleA(a, b, c, &A) == 1)
    {
        printf("Hoek A = %f rad", A);
    }
    else
    {
        printf("Met deze drie lijnen kan geen driehoek worden gevormd.");
    }

    return 0;
}
