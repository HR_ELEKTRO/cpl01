#include <stdio.h>

void print(int p[18][18], char char0, char char1)
{
    for (int r = 0; r < 18; r++)
    {
        for (int c = 0; c < 18; c++)
        {
            if (p[r][c] == 0)
            {
                printf("%c", char0);
            }
            else
            {
                printf("%c", char1);
            }
        }
        printf("\n");
    }
}

int isSymmetrischInYAs(int p[18][18]) {
    for (int r = 0; r < 18; r++)
    {
        for(int c = 0; c < 9; c++)
        {
            if (p[r][c] != p[r][17-c])
            {
                return 0;
            }
        }
    }
    return 1;
}

int main(void)
{
    int plaatje[18][18] =
    {
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,0},
        {1,1,1,1,1,0,1,1,1,1,1,1,0,1,1,1,1,1},
        {1,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1},
        {1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1},
        {0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0},
        {0,0,1,0,0,1,1,1,0,0,1,1,1,0,0,1,0,0},
        {0,1,0,0,1,1,1,1,0,0,1,1,1,1,0,0,1,0},
        {0,1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,0},
        {0,1,0,0,1,1,1,1,0,0,1,1,1,1,0,0,1,0},
        {0,1,0,0,1,1,1,0,0,0,0,1,1,1,0,0,1,0},
        {0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0},
        {0,1,0,0,0,0,0,1,1,1,1,0,0,0,0,0,1,0},
        {0,0,1,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0},
        {0,0,1,0,0,0,1,0,0,0,0,1,0,0,0,1,0,0},
        {0,0,0,1,0,0,0,1,1,1,1,0,0,0,1,0,0,0},
        {0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0,0},
        {0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0}
    };
    printf("Een panda:\n");
    print(plaatje, ' ', '8');
    if (isSymmetrischInYAs(plaatje))
    {
        printf("Het plaatje van de panda is inderdaad symmetrisch in de Y-as.\n");
    }
    else
    {
        printf("Klopt niet!");
    }

    plaatje[13][9] = 0;
    printf("Een panda met een scheve neus:\n");
    print(plaatje, ' ', 'X');
    if (isSymmetrischInYAs(plaatje))
    {
        printf("Klopt niet!");
    }
    else
    {
        printf("Een panda met een scheve neus is inderdaad niet symmetrisch in de Y-as.\n");
    }

    return 0;
}
