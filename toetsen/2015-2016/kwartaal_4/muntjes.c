#include <stdio.h>

int main(void)
{
    int muntje, aantalMuntjes20 = 0, aantalMuntjes50 = 0;
    printf("Voer muntjes in:\n");
    do
    {
        scanf("%d", &muntje);
        if (muntje == 20)
        {
            aantalMuntjes20++;
        }
        else if (muntje == 50)
        {
            aantalMuntjes50++;
        }
        else if (muntje >= 0) // voorkom dat er bij invoer negatief getal melding wordt gegeven
        {
            printf("Ongeldig muntje!\n");
        }
    }
    while (muntje >= 0);
    printf("Het aantal muntjes is %d, met een waarde van %.2f euro.\n", aantalMuntjes20 + aantalMuntjes50, aantalMuntjes20 * 0.2 + aantalMuntjes50 * 0.5);
    printf("20: ");
    for (int i = 0; i < aantalMuntjes20; i++)
    {
        printf("+");
    }
    printf("\n50: ");
    for (int i = 0; i < aantalMuntjes50; i++)
    {
        printf("+");
    }
    printf("\n");
    return 0;
}
