#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*
 * gooiDobbelsteen() geeft een willekeurige waarde tussen 1 en 6 terug.
 * Deze functie kun je gebruiken bij de paardenrace.
 *
 * De code van deze functie hoef je niet te begrijpen of aan te passen.
 *
 */
int gooiDobbelsteen(void)
{
    static int init = 1;
    if (init)
    {
        srand(time(NULL));
        init = 0;
    }

    return (rand() % 6) + 1;
}

void printPaardVoortgang(char *naam, int aantalStappen)
{


}

int main(void)
{


    return 0;
}

