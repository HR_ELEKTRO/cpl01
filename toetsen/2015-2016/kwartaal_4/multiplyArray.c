#include <stdio.h>

void multiplyArray(double *x, double *y, double *z, double matrix[3][3])
{
    double x_uit = *x * matrix[0][0] + *y * matrix[0][1] + *z * matrix[0][2];
    double y_uit = *x * matrix[1][0] + *y * matrix[1][1] + *z * matrix[1][2];
    double z_uit = *x * matrix[2][0] + *y * matrix[2][1] + *z * matrix[2][2];
    *x = x_uit;
    *y = y_uit;
    *z = z_uit;
}

int main(void)
{
    double x = 0.1, y = 0.2, z = 0.3;
    double m[3][3] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

    multiplyArray(&x, &y, &z, m);

    printf("%f, %f, %f\n", x, y, z);
    return 0;
}
