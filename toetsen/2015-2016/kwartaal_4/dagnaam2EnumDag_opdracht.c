#include <stdio.h>
#include <string.h>
#include <ctype.h>

enum dag {ONBEKEND, MAANDAG, DINSDAG, WOENSDAG, DONDERDAG, VRIJDAG, ZATERDAG, ZONDAG};

enum dag dagnaam2EnumDag(const char *dagnaam)
{
        /* vul hier je code in */
}

int main(void)
{
    char naam[10];
    printf("Geef de naam van een dag: ");
    scanf("%10s", &naam);
    switch(dagnaam2EnumDag(naam))
    {
    case MAANDAG:
        printf("%s is wasdag.\n", naam);
        break;
    case DINSDAG:
        printf("%s is strijkdag.\n", naam);
        break;
    case WOENSDAG:
        printf("%s is gehakt- en versteldag.\n", naam);
        break;
    case DONDERDAG:
        printf("%s is kuisdag.\n", naam);
        break;
    case VRIJDAG:
        printf("%s is visdag.\n", naam);
        break;
    case ZATERDAG:
        printf("%s is klusjesdag.\n", naam);
        break;
    case ZONDAG:
        printf("%s is kerkdag.\n", naam);
        break;
    default:
        printf("%s is geen dag.\n", naam);
        break;
    }
    return 0;
}
