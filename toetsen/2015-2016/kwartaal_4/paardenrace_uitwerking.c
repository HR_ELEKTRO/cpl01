#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*
 * gooiDobbelsteen() geeft een willekeurige waarde tussen 1 en 6 terug.
 * Deze functie kun je gebruiken bij de paardenrace.
 *
 * De code van deze functie hoef je niet te begrijpen of aan te passen.
 *
 */
int gooiDobbelsteen()
{
    static int first = 1;
    if(first)
    {
        srand(time(NULL));
        first = 0;
    }

    return (rand() % 6) + 1;
}

void printPaardVoortgang(char *naam, int aantalStappen)
{
    printf("%-9s: ", naam);
    for(int i = 0; i < aantalStappen; i++)
    {
        printf(">");
    }
    printf("\n");
}

int main1()
{
    char paard1Naam[10];
    char paard2Naam[10];
    char paard3Naam[10];

    int paard1Stappen = 0;
    int paard2Stappen = 0;
    int paard3Stappen = 0;

    int beurten = 0;

    printf("Geef de namen van de 3 paarden:\n");
    scanf("%9s", paard1Naam);
    scanf("%9s", paard2Naam);
    scanf("%9s", paard3Naam);

    printf("\nWelkom %s, %s en %s!\n\n", paard1Naam, paard2Naam, paard3Naam);

    while(paard1Stappen < 40 && paard2Stappen < 40 && paard3Stappen < 40)
    {
        beurten++;
        paard1Stappen += gooiDobbelsteen();
        paard2Stappen += gooiDobbelsteen();
        paard3Stappen += gooiDobbelsteen();
        printPaardVoortgang(paard1Naam, paard1Stappen);
        printPaardVoortgang(paard2Naam, paard2Stappen);
        printPaardVoortgang(paard3Naam, paard3Stappen);
        printf("\n");
    }

    if(paard1Stappen > paard2Stappen && paard1Stappen > paard3Stappen)
    {
        printf("%s heeft gewonnen in %d beurten.\n\n", paard1Naam, beurten);
    }
    else if(paard2Stappen > paard1Stappen && paard2Stappen > paard3Stappen)
    {
        printf("%s heeft gewonnen in %d beurten.\n\n", paard2Naam, beurten);
    }
    else if(paard3Stappen > paard1Stappen && paard3Stappen > paard2Stappen)
    {
        printf("%s heeft gewonnen in %d beurten.\n\n", paard3Naam, beurten);
    }
    else
    {
        printf("Er was een gelijkspel!\n\n");
    }

    return 0;
}

int main()
{
    char namen[3][10];
    int stappen[3] = {0, 0, 0};
    int beurten = 0;

    printf("Geef de namen van de 3 paarden:\n");
    for(int i = 0; i < 3; i++)
    {
        scanf("%9s", namen[i]);
    }

    printf("\nWelkom %s, %s en %s!\n\n", namen[0], namen[1], namen[2]);

    while(stappen[0] < 40 && stappen[1] < 40 && stappen[2] < 40)
    {
        beurten++;
        for(int i = 0; i < 3; i++)
        {
            stappen[i] += gooiDobbelsteen();
            printPaardVoortgang(namen[i], stappen[i]);
        }
        printf("\n");
    }

    if(stappen[0] > stappen[1] && stappen[0] > stappen[2])
    {
        printf("%s heeft gewonnen in %d beurten.\n\n", namen[0], beurten);
    }
    else if(stappen[1] > stappen[0] && stappen[1] > stappen[2])
    {
        printf("%s heeft gewonnen in %d beurten.\n\n", namen[1], beurten);
    }
    else if(stappen[2] > stappen[0] && stappen[2] > stappen[1])
    {
        printf("%s heeft gewonnen in %d beurten.\n\n", namen[2], beurten);
    }
    else
    {
        printf("Er was een gelijkspel!\n\n");
    }

    return 0;
}
