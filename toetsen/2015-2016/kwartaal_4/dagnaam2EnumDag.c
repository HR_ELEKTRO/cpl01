#include <stdio.h>
#include <string.h>
#include <ctype.h>

void makeLowerCase(char *s)
{
    for (int i = 0; s[i] != '\0'; i++)
    {
        s[i] = tolower(s[i]);
    }
}

enum dag {ONBEKEND, MAANDAG, DINSDAG, WOENSDAG, DONDERDAG, VRIJDAG, ZATERDAG, ZONDAG};

enum dag dagnaam2EnumDag(const char *dagnaam)
{
    char dagnaam2[strlen(dagnaam) + 1];
    strcpy(dagnaam2, dagnaam);
    makeLowerCase(dagnaam2);
    if (strcmp(dagnaam2, "maandag") == 0)
        return MAANDAG;
    if (strcmp(dagnaam2, "dinsdag") == 0)
        return DINSDAG;
    if (strcmp(dagnaam2, "woensdag") == 0)
        return WOENSDAG;
    if (strcmp(dagnaam2, "donderdag") == 0)
        return DONDERDAG;
    if (strcmp(dagnaam2, "vrijdag") == 0)
        return VRIJDAG;
    if (strcmp(dagnaam2, "zaterdag") == 0)
        return ZATERDAG;
    if (strcmp(dagnaam2, "zondag") == 0)
        return ZONDAG;
    return ONBEKEND;
}

int main(void)
{
    char naam[10];
    printf("Geef de naam van een dag: ");
    scanf("%10s", &naam);
    switch(dagnaam2EnumDag(naam))
    {
    case MAANDAG:
        printf("%s is wasdag.\n", naam);
        break;
    case DINSDAG:
        printf("%s is strijkdag.\n", naam);
        break;
    case WOENSDAG:
        printf("%s is gehakt- en versteldag.\n", naam);
        break;
    case DONDERDAG:
        printf("%s is kuisdag.\n", naam);
        break;
    case VRIJDAG:
        printf("%s is visdag.\n", naam);
        break;
    case ZATERDAG:
        printf("%s is klusjesdag.\n", naam);
        break;
    case ZONDAG:
        printf("%s is kerkdag.\n", naam);
        break;
    default:
        printf("%s is geen dag.\n", naam);
        break;
    }
    return 0;
}
