#include <stdio.h>

int main(void)
{
    int muntje, aantalMuntjes20 = 0, aantalMuntjes50 = 0;
    printf("Voer muntjes in:\n");
    do
    {
        scanf("%d", &muntje);
        if (muntje == 20)
        {
            aantalMuntjes20++;
        }
        else if (muntje == 50)
        {
            aantalMuntjes50++;
        }
        else if (muntje >= 0) // voorkom dat er bij invoer negatief getal melding wordt gegeven
        {
            printf("Ongeldig muntje!\n");
        }
    }
    while (muntje >= 0);
    printf("Het aantal muntjes is %d, met een waarde van %.2f euro.\n", aantalMuntjes20 + aantalMuntjes50, aantalMuntjes20 * 0.2 + aantalMuntjes50 * 0.5);
    char dummy;
    scanf("%c", &dummy); // Return karakter dat is ingetypt na het laatste getal inlezen en weggooien.
    char karakterVoor20, karakterVoor50;
    printf("Welk karakter wil je printen voor 20 cent? ");
    scanf("%c", &karakterVoor20);
    scanf("%c", &dummy); // Return karakter dat is ingetypt na het karakter voor 20 cent inlezen en weggooien.
    printf("Welk karakter wil je printen voor 50 cent? ");
    scanf("%c", &karakterVoor50);
    printf("20: ");
    for (int i = 0; i < aantalMuntjes20; i++)
    {
        printf("%c", karakterVoor20);
    }
    printf("\n50: ");
    for (int i = 0; i < aantalMuntjes50; i++)
    {
        printf("%c", karakterVoor50);
    }
    printf("\n");
    return 0;
}
