#include <stdio.h>

/* Copyright 2016 Hogeschool Rotterdam */
/* Dit programma leest een geheel getal 0 < n < 7 en drukt
   vervolgens de tafels van 1 t/m n naast elkaar af */

int main(void)
{
    int n;

    do
    {
        printf("Geef de waarde van n (1..6): ");
        fflush(stdin);
    }
    while (scanf("%d", &n) != 1 || n < 1 || n > 6);

    for (int factor = 1; factor < 11; factor = factor + 1)
    {
        for (int tafel = 1; tafel < n + 1; tafel = tafel + 1)
        {
            printf("%2d x %d = %2d ", factor, tafel, factor * tafel);
        }
        printf("\n");
    }

    return 0;
}

