#include <stdio.h>

int main(void)
{
    int xtoy(int x, int y);
    printf("Debug Demonstratie Code::Blocks\n");
    int macht = xtoy(7, 8);
    printf("\n7 tot de macht 8 = %d", macht);
    return 0;
}

int xtoy(int x, int y)
{
    int answer = x;
    for(int tel = 0; tel < y; tel++)
    {
        answer = answer * x;
    }
    return answer;
}
