#include <stdio.h>

int main(void)
{
    int array[] = {3, 5, 6, 9};
    int telOp(int rij[], int aantal);

    printf("Debug Demonstratie Code::Blocks\n");
    int som = telOp(array, sizeof(array)/sizeof(array[0]));
    printf("De som van de 4 getallen is: %d", som);
    return 0;
}

int telOp(int rij[], int aantal)
{
    int answer;
    for (int tel = 0; tel < aantal; tel++)
    {
        answer = answer + rij[tel];
    }
    return answer;
}
