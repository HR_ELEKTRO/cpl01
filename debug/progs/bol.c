#include <stdio.h>

int main(void)
{
    double inhoud;
    int straal = 2;
    double vier_pi = (4 * 3,1415926535897932);

    printf("Debug Demonstratie Code::Blocks\n");

    inhoud = (straal * straal * straal * vier_pi) / 3;
    printf("Een bol met een straal van %d cm heeft een inhoud van %f cm^3", straal, inhoud);
    return 0;
}