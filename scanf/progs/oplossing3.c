#include <stdio.h>

int main(void)
{
    int getal;

    printf("Geef een geheel getal: ");
    while (scanf("%d", &getal) != 1)
    {
        char karakter;
        do
        {
            scanf("%c", &karakter);
        }
        while (karakter != '\n');
        printf("Geef een geheel getal: ");
    }
    printf("Het ingelezen getal is %d.\n", getal);
    return 0;
}
