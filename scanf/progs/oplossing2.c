#include <stdio.h>

int main(void)
{
    int getal;
    do
    {
        printf("Geef een geheel getal: ");
        fflush(stdin);
    }
    while (scanf("%d", &getal) != 1);
    printf("Het ingelezen getal is %d.\n", getal);
    return 0;
}
