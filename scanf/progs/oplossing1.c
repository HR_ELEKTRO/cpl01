#include <stdio.h>

int main(void)
{
    int getal, ret;
    do
    {
        printf("Geef een geheel getal: ");
        ret = scanf("%d", &getal);
        if (ret == 0)
        {
            printf("Dat was geen getal!\n");
            char karakter;
            scanf("%c", &karakter);
            printf("Maar het karakter %c.\n", karakter);
        }
        else if (ret == EOF)
        {
            printf("Er is een fout opgetreden bij het lezen!\n");
        }
    }
    while (ret != 1);
    printf("Het ingelezen getal is %d.\n", getal);
    return 0;
}
