# CPL01 - Programmeren in C #

Dit repository is bedoeld voor studenten en docenten van de opleiding Elektrotechniek van de Hogeschool Rotterdam en wordt gebruikt om aanvullend studiemateriaal voor de module "CPL01 - Programmeren in C" te ontwikkelen en te verspreiden. 

Alle informatie is te vinden op de [Wiki](https://bitbucket.org/HR_ELEKTRO/cpl01/wiki/).

Deze module wordt niet meer aangeboden en is voor het laatst in het studiejaar 2017-2018 gegeven.