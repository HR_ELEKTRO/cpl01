#include <stdio.h>
#include <stdbool.h>
#include <math.h>

bool isPrime(int n)
{
    if (n < 2)
    {
        return false;
    }
    for (int i = 2; i <= sqrt(n); i++)
    {
        if (n % i == 0)
        {
            return false;
        }
    }
    return true;
}

void primesAround(int n, int *p_lower, int *p_higher)
{
    if (n > 2)
    {
        int lower = n;
        do
        {
            lower = lower - 1;
        } while (isPrime(lower) == false);
        {
        }
        int higher = n;
        do
        {
            higher = higher + 1;
        } while (isPrime(higher) == false);
        *p_lower = lower;
        *p_higher = higher;
    }
}

int main(void)
{
    int number;
    printf("Geef een geheel number: ");
    scanf("%d", &number);
    printf("Het number %d is ", number);
    if (isPrime(number) == true)
    {
        printf("een ");
    }
    else
    {
        printf("geen ");
    }
    printf("priemgetal\n");
    int low, high;
    primesAround(number, &low, &high);
    printf("Het grootste priemgetal kleiner dan %d is %d.\n", number, low);
    printf("Het kleinste priemgetal groter dan %d is %d.\n", number, high);
    return 0;
}
