#include <stdio.h>
#include <stdbool.h>
#include <math.h>

bool isPrime(int n)
{
    if (n < 2)
    {
        return false;
    }
    for (int i = 2; i <= sqrt(n); i++)
    {
        if (n % i == 0)
        {
            return false;
        }
    }
    return true;
}

int main(void)
{
    int number;
    printf("Geef een geheel number: ");
    scanf("%d", &number);
    printf("Het number %d is ", number);
    if (isPrime(number) == true)
    {
        printf("een ");
    }
    else
    {
        printf("geen ");
    }
    printf("priemgetal\n");
    return 0;
}
