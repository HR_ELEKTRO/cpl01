#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#define DIM 12

bool areRowsValid(int m[][DIM])
{
    for (int r = 0; r < DIM; r = r + 1)
    {
        int numberOf[3] = {0};
        int lastBit = 2;
        int successiveEqualBits = 0;
        for (int c = 0; c < DIM; c = c + 1)
        {
            int bit = m[r][c];
            if (bit != 2)
            {
                if (successiveEqualBits == 0)
                {
                    successiveEqualBits = 1;
                }
                else
                {
                    if (bit == lastBit)
                    {
                        successiveEqualBits++;
                        if (successiveEqualBits == 3)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        successiveEqualBits = 1;
                    }
                }
            }
            else
            {
                successiveEqualBits = 0;
            }
            assert(bit >= 0 && bit <= 2);
            numberOf[bit]++;
            lastBit = bit;
        }
        if (numberOf[0] > DIM/2 || numberOf[1] > DIM/2)
        {
            return false;
        }
    }
    return true;
}

bool areColumnsValid(int m[][DIM])
{
    for (int c = 0; c < DIM; c = c + 1)
    {
        int numberOf[3] = {0};
        int lastBit = 2;
        int successiveEqualBits = 0;
        for (int r = 0; r < DIM; r = r + 1)
        {
            int bit = m[r][c];
            if (bit != 2)
            {
                if (successiveEqualBits == 0)
                {
                    successiveEqualBits = 1;
                }
                else
                {
                    if (bit == lastBit)
                    {
                        successiveEqualBits++;
                        if (successiveEqualBits == 3)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        successiveEqualBits = 1;
                    }
                }
            }
            else
            {
                successiveEqualBits = 0;
            }
            assert(bit >= 0 && bit <= 2);
            numberOf[bit]++;
            lastBit = bit;
        }
        if (numberOf[0] > DIM/2 || numberOf[1] > DIM/2)
        {
            return false;
        }
    }
    return true;
}

bool isValid(int m[][DIM])
{
    return areRowsValid(m) && areColumnsValid(m);
}

bool solveLow(int m[][DIM])
{
    for (int r = 0; r < DIM; r = r + 1)
    {
        for (int c = 0; c < DIM; c = c + 1)
        {
            if (m[r][c] == 2)
            {
                for (int bit = 0; bit <= 1; bit = bit + 1)
                {
                    m[r][c] = bit;
                    if (isValid(m) && solveLow(m))
                    {
                        return true;
                    }
                    m[r][c] = 2;
                }
                return false;
            }
        }
    }
    return isValid(m);
}

bool solveHigh(int m[][DIM])
{
    for (int r = 0; r < DIM; r = r + 1)
    {
        for (int c = 0; c < DIM; c = c + 1)
        {
            if (m[r][c] == 2)
            {
                for (int bit = 1; bit >= 0; bit = bit - 1)
                {
                    m[r][c] = bit;
                    if (isValid(m) && solveHigh(m))
                    {
                        return true;
                    }
                    m[r][c] = 2;
                }
                return false;
            }
        }
    }
    return isValid(m);
}

void printPuzzle(int m[][DIM])
{
    for (int r = 0; r < DIM; r = r + 1)
    {
        for (int c = 0; c < DIM; c = c + 1)
        {
            if (m[r][c] == 2)
            {
                printf("  ");
            }
            else
            {
                printf("%d ", m[r][c]);
            }
        }
        printf("\n");
    }
}

bool isEqual(int m1[][DIM], int m2[][DIM])
{
    for (int r = 0; r < DIM; r = r + 1)
    {
        for (int c = 0; c < DIM; c = c + 1)
        {
            if (m1[r][c] != m2[r][c])
            {
                return false;
            }
        }
    }
    return true;
}

void copy(int to[][DIM], int from[][DIM])
{
    for (int r = 0; r < DIM; r = r + 1)
    {
        for (int c = 0; c < DIM; c = c + 1)
        {
            to[r][c] = from[r][c];
        }
    }
}

bool solve(int puzzle[][DIM])
{
    int result1[DIM][DIM], result2[DIM][DIM];
    copy(result1, puzzle);
    if (solveLow(result1))
    {
        copy(result2, puzzle);
        if (solveHigh(result2) && isEqual(result1, result2))
        {
            copy(puzzle, result1);
            return true;
        }
    }
    return false;
}

void printTestResult(int testNumber, int testResult)
{
    if (testResult)
    {
        printf("Test %d is succesvol uitgevoerd.\n", testNumber);
    }
    else
    {
        printf("Test %d is NIET succesvol uitgevoerd!\n", testNumber);
    }
}

int main(void)
{
    int puzzle0[DIM][DIM] =
    {
        {2 ,2, 2, 2 ,2 ,2, 2, 2, 2, 2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2, 2, 2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2, 2, 2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2, 2, 2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2, 2, 2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2, 2, 2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2, 2, 2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2, 2, 2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2, 2, 2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2, 2, 2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2, 2, 2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2, 2, 2, 2, 2}
    };
    int puzzle1[DIM][DIM] =
    {
        {2 ,2, 1, 2 ,0 ,2, 2, 1, 2, 1, 2, 1},
        {2 ,2, 1, 2 ,2 ,2, 2, 2, 0, 2, 0, 2},
        {0 ,2, 2, 0 ,2 ,2, 2, 2, 2, 2, 2, 0},
        {2 ,1, 1, 2 ,2 ,0, 2, 2, 2, 0, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2, 2, 2, 2, 2},
        {2 ,0, 2, 2 ,2 ,2, 0, 2, 2, 2, 2, 2},
        {1 ,2, 2, 2 ,2 ,2, 0, 2, 2, 1, 2, 0},
        {2 ,0, 2, 2 ,2 ,2, 2, 2, 2, 1, 2, 2},
        {2 ,2, 1, 2 ,2 ,2, 2, 2, 2, 2, 2, 0},
        {2 ,2, 2, 2 ,2 ,2, 0, 2, 1, 2, 0, 2},
        {1 ,2, 2, 1 ,2 ,2, 2, 2, 2, 2, 2, 2},
        {2 ,0, 0, 2 ,0 ,2, 2, 1, 2, 2, 0, 2}
    };

    printTestResult(1, solve(puzzle0) == false);
    printTestResult(2, solve(puzzle1) == true);

    int puzzle[DIM][DIM];
    copy(puzzle, puzzle1);
    solveLow(puzzle);
    printf("solveLow(puzzle1):\n");
    printPuzzle(puzzle);

    copy(puzzle, puzzle1);
    solveHigh(puzzle);
    printf("solveHigh(puzzle1):\n");
    printPuzzle(puzzle);

    copy(puzzle, puzzle0);
    solveLow(puzzle);
    printf("solveLow(puzzle0):\n");
    printPuzzle(puzzle);

    copy(puzzle, puzzle0);
    solveHigh(puzzle);
    printf("solveHigh(puzzle0):\n");
    printPuzzle(puzzle);

    return 0;
}

