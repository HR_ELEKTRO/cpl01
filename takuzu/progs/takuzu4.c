#include <stdio.h>
#include <stdbool.h>

bool areRowsValid(int m[][4])
{
    for (int r = 0; r < 4; r = r + 1)
    {
        int numberOf[3] = {0};
        for (int c = 0; c < 4; c = c + 1)
        {
            numberOf[m[r][c]]++;
        }
        if (numberOf[0]>2 || numberOf[1] > 2)
        {
            return false;
        }
    }
    return true;
}

bool areColumnsValid(int m[][4])
{
    for (int c = 0; c < 4; c = c + 1)
    {
        int numberOf[3] = {0};
        for (int r = 0; r < 4; r = r + 1)
        {
            numberOf[m[r][c]]++;
        }
        if (numberOf[0]>2 || numberOf[1] > 2)
        {
            return false;
        }
    }
    return true;
}

bool isValid(int m[][4])
{
    return areRowsValid(m) && areColumnsValid(m);
}

bool solve(int m[][4])
{
    for (int r = 0; r < 4; r = r + 1)
    {
        for (int c = 0; c < 4; c = c + 1)
        {
            if (m[r][c] == 2)
            {
                for (int digit = 0; digit <= 1; digit = digit + 1)
                {
                    m[r][c] = digit;
                    if (isValid(m) && solve(m))
                    {
                        return true;
                    }
                    m[r][c] = 2;
                }
                return false;
            }
        }
    }
    return isValid(m);
}

bool savePuzzle(FILE* fp, int m[][4])
{
    for (int r = 0; r < 4; r = r + 1)
    {
        for (int c = 0; c < 4; c = c + 1)
        {
            if (m[r][c] == 2)
            {
                if (fprintf(fp, "  ") <= 0)
                {
                    return false;
                }
            }
            else
            {
                if (fprintf(fp, "%d ", m[r][c]) <= 0)
                {
                    return false;
                }
            }
        }
        if (fprintf(fp, "\n") <= 0)
        {
            return false;
        }
    }
    return true;
}

bool isEqual(int m1[][4], int m2[][4])
{
    for (int r = 0; r < 4; r = r + 1)
    {
        for (int c = 0; c < 4; c = c + 1)
        {
            if (m1[r][c] != m2[r][c])
            {
                return false;
            }
        }
    }
    return true;
}

void printTestResult(int testNumber, int testResult)
{
    if (testResult)
    {
        printf("Test %d is succesvol uitgevoerd.\n", testNumber);
    }
    else
    {
        printf("Test %d is NIET succesvol uitgevoerd!\n", testNumber);
    }
}

int main(void)
{
    int puzzle1[4][4] =
    {
        {2 ,1 ,2 ,0},
        {2 ,2 ,0 ,2},
        {2 ,0 ,2 ,2},
        {1 ,1 ,2 ,0}
    };
    int solution1[4][4] =
    {
        {0, 1, 1, 0},
        {1, 0, 0, 1},
        {0, 0, 1, 1},
        {1, 1, 0, 0}
    };

    solve(puzzle1);
    savePuzzle(stdout, puzzle1);
    printTestResult(1, isEqual(puzzle1, solution1) == true);

    return 0;
}

