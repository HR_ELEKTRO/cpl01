#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

bool areRowsValid(int m[][8])
{
    for (int r = 0; r < 8; r = r + 1)
    {
        int numberOf[3] = {0};
        int lastBit = 2;
        int successiveEqualBits = 0;
        for (int c = 0; c < 8; c = c + 1)
        {
            int bit = m[r][c];
            if (bit != 2)
            {
                if (successiveEqualBits == 0)
                {
                    successiveEqualBits = 1;
                }
                else
                {
                    if (bit == lastBit)
                    {
                        successiveEqualBits++;
                        if (successiveEqualBits == 3)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        successiveEqualBits = 1;
                    }
                }
            }
            else
            {
                successiveEqualBits = 0;
            }
            assert(bit >= 0 && bit <= 2);
            numberOf[bit]++;
            lastBit = bit;
        }
        if (numberOf[0] > 4 || numberOf[1] > 4)
        {
            return false;
        }
    }
    return true;
}

bool areColumnsValid(int m[][8])
{
    for (int c = 0; c < 8; c = c + 1)
    {
        int numberOf[3] = {0};
        int lastBit = 2;
        int successiveEqualBits = 0;
        for (int r = 0; r < 8; r = r + 1)
        {
            int bit = m[r][c];
            if (bit != 2)
            {
                if (successiveEqualBits == 0)
                {
                    successiveEqualBits = 1;
                }
                else
                {
                    if (bit == lastBit)
                    {
                        successiveEqualBits++;
                        if (successiveEqualBits == 3)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        successiveEqualBits = 1;
                    }
                }
            }
            else
            {
                successiveEqualBits = 0;
            }
            assert(bit >= 0 && bit <= 2);
            numberOf[bit]++;
            lastBit = bit;
        }
        if (numberOf[0] > 4 || numberOf[1] > 4)
        {
            return false;
        }
    }
    return true;
}

bool isValid(int m[][8])
{
    return areRowsValid(m) && areColumnsValid(m);
}

bool solveLow(int m[][8])
{
    for (int r = 0; r < 8; r = r + 1)
    {
        for (int c = 0; c < 8; c = c + 1)
        {
            if (m[r][c] == 2)
            {
                for (int bit = 0; bit <= 1; bit = bit + 1)
                {
                    m[r][c] = bit;
                    if (isValid(m) && solveLow(m))
                    {
                        return true;
                    }
                    m[r][c] = 2;
                }
                return false;
            }
        }
    }
    return isValid(m);
}

bool solveHigh(int m[][8])
{
    for (int r = 0; r < 8; r = r + 1)
    {
        for (int c = 0; c < 8; c = c + 1)
        {
            if (m[r][c] == 2)
            {
                for (int bit = 1; bit >= 0; bit = bit - 1)
                {
                    m[r][c] = bit;
                    if (isValid(m) && solveHigh(m))
                    {
                        return true;
                    }
                    m[r][c] = 2;
                }
                return false;
            }
        }
    }
    return isValid(m);
}

void printPuzzle(int m[][8])
{
    for (int r = 0; r < 8; r = r + 1)
    {
        for (int c = 0; c < 8; c = c + 1)
        {
            if (m[r][c] == 2)
            {
                printf("  ");
            }
            else
            {
                printf("%d ", m[r][c]);
            }
        }
        printf("\n");
    }
}

bool isEqual(int m1[][8], int m2[][8])
{
    for (int r = 0; r < 8; r = r + 1)
    {
        for (int c = 0; c < 8; c = c + 1)
        {
            if (m1[r][c] != m2[r][c])
            {
                return false;
            }
        }
    }
    return true;
}

void copy(int to[][8], int from[][8])
{
    for (int r = 0; r < 8; r = r + 1)
    {
        for (int c = 0; c < 8; c = c + 1)
        {
            to[r][c] = from[r][c];
        }
    }
}

bool solve(int puzzle[][8])
{
    int result1[8][8], result2[8][8];
    copy(result1, puzzle);
    if (solveLow(result1))
    {
        copy(result2, puzzle);
        if (solveHigh(result2) && isEqual(result1, result2))
        {
            copy(puzzle, result1);
            return true;
        }
    }
    return false;
}

void printTestResult(int testNumber, int testResult)
{
    if (testResult)
    {
        printf("Test %d is succesvol uitgevoerd.\n", testNumber);
    }
    else
    {
        printf("Test %d is NIET succesvol uitgevoerd!\n", testNumber);
    }
}

int main(void)
{
    int puzzle0[8][8] =
    {
        {2 ,2, 2, 2 ,2 ,2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2},
        {2 ,2, 2, 2 ,2 ,2, 2, 2}
    };
    int puzzle1[8][8] =
    {
        {0 ,2, 2, 1 ,2 ,2, 2, 2},
        {2 ,1, 2, 2 ,0 ,2, 2, 1},
        {2 ,2, 2, 2 ,2 ,1, 2, 1},
        {1 ,2, 2, 0 ,2 ,2, 2, 2},
        {2 ,2, 0, 2 ,2 ,1, 2, 2},
        {2 ,2, 0, 1 ,2 ,2, 2, 0},
        {0 ,2, 2, 1 ,2 ,1, 1, 2},
        {1 ,0, 2, 2 ,2 ,0, 2, 2}
    };

    printTestResult(1, solve(puzzle0) == false);
    printTestResult(2, solve(puzzle1) == true);

    int puzzle[8][8];
    copy(puzzle, puzzle1);
    solveLow(puzzle);
    printf("solveLow(puzzle1):\n");
    printPuzzle(puzzle);

    copy(puzzle, puzzle1);
    solveHigh(puzzle);
    printf("solveHigh(puzzle1):\n");
    printPuzzle(puzzle);

    copy(puzzle, puzzle0);
    solveLow(puzzle);
    printf("solveLow(puzzle0):\n");
    printPuzzle(puzzle);

    copy(puzzle, puzzle0);
    solveHigh(puzzle);
    printf("solveHigh(puzzle0):\n");
    printPuzzle(puzzle);

    return 0;
}

