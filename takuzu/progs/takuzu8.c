#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

bool areRowsValid(int m[][8])
{
    for (int r = 0; r < 8; r = r + 1)
    {
        int numberOf[3] = {0};
        int lastBit = 2;
        int successiveEqualBits = 0;
        for (int c = 0; c < 8; c = c + 1)
        {
            int bit = m[r][c];
            if (bit != 2)
            {
                if (successiveEqualBits == 0)
                {
                    successiveEqualBits = 1;
                }
                else
                {
                    if (bit == lastBit)
                    {
                        successiveEqualBits++;
                        if (successiveEqualBits == 3)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        successiveEqualBits = 1;
                    }
                }
            }
            else
            {
                successiveEqualBits = 0;
            }
            assert(bit >= 0 && bit <= 2);
            numberOf[bit]++;
            lastBit = bit;
        }
        if (numberOf[0] > 4 || numberOf[1] > 4)
        {
            return false;
        }
    }
    return true;
}

bool areColumnsValid(int m[][8])
{
    for (int c = 0; c < 8; c = c + 1)
    {
        int numberOf[3] = {0};
        int lastBit = 2;
        int successiveEqualBits = 0;
        for (int r = 0; r < 8; r = r + 1)
        {
            int bit = m[r][c];
            if (bit != 2)
            {
                if (successiveEqualBits == 0)
                {
                    successiveEqualBits = 1;
                }
                else
                {
                    if (bit == lastBit)
                    {
                        successiveEqualBits++;
                        if (successiveEqualBits == 3)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        successiveEqualBits = 1;
                    }
                }
            }
            else
            {
                successiveEqualBits = 0;
            }
            assert(bit >= 0 && bit <= 2);
            numberOf[bit]++;
            lastBit = bit;
        }
        if (numberOf[0] > 4 || numberOf[1] > 4)
        {
            return false;
        }
    }
    return true;
}

bool isValid(int m[][8])
{
    return areRowsValid(m) && areColumnsValid(m);
}

bool solve(int m[][8])
{
    for (int r = 0; r < 8; r = r + 1)
    {
        for (int c = 0; c < 8; c = c + 1)
        {
            if (m[r][c] == 2)
            {
                for (int bit = 0; bit <= 1; bit = bit + 1)
                {
                    m[r][c] = bit;
                    if (isValid(m) && solve(m))
                    {
                        return true;
                    }
                    m[r][c] = 2;
                }
                return false;
            }
        }
    }
    return isValid(m);
}

void printPuzzle(int m[][8])
{
    for (int r = 0; r < 8; r = r + 1)
    {
        for (int c = 0; c < 8; c = c + 1)
        {
            if (m[r][c] == 2)
            {
                printf("  ");
            }
            else
            {
                printf("%d ", m[r][c]);
            }
        }
        printf("\n");
    }
}

bool isEqual(int m1[][8], int m2[][8])
{
    for (int r = 0; r < 8; r = r + 1)
    {
        for (int c = 0; c < 8; c = c + 1)
        {
            if (m1[r][c] != m2[r][c])
            {
                return false;
            }
        }
    }
    return true;
}

void printTestResult(int testNumber, int testResult)
{
    if (testResult)
    {
        printf("Test %d is succesvol uitgevoerd.\n", testNumber);
    }
    else
    {
        printf("Test %d is NIET succesvol uitgevoerd!\n", testNumber);
    }
}

int main(void)
{
    int puzzle1[8][8] =
    {
        {0 ,2, 2, 1 ,2 ,2, 2, 2},
        {2 ,1, 2, 2 ,0 ,2, 2, 1},
        {2 ,2, 2, 2 ,2 ,1, 2, 1},
        {1 ,2, 2, 0 ,2 ,2, 2, 2},
        {2 ,2, 0, 2 ,2 ,1, 2, 2},
        {2 ,2, 0, 1 ,2 ,2, 2, 0},
        {0 ,2, 2, 1 ,2 ,1, 1, 2},
        {1 ,0, 2, 2 ,2 ,0, 2, 2}
    };
    int solution1[8][8] =
    {
        {0 ,1, 0, 1 ,1 ,0, 1, 0},
        {0 ,1, 1, 0 ,0 ,1, 0, 1},
        {1 ,0, 0, 1 ,0 ,1, 0, 1},
        {1 ,0, 1, 0 ,1 ,0, 1, 0},
        {0 ,1, 0, 0 ,1 ,1, 0, 1},
        {1 ,1, 0, 1 ,0 ,0, 1, 0},
        {0 ,0, 1, 1 ,0 ,1, 1, 0},
        {1 ,0, 1, 0 ,1 ,0, 0, 1}
    };

    solve(puzzle1);
    printPuzzle(puzzle1);
    printTestResult(1, isEqual(puzzle1, solution1) == true);

    return 0;
}

