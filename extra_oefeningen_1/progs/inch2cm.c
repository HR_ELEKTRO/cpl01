#include <stdio.h>

int main(void)
{
    double lengteInInches;
    printf("Geef lengte in inches: ");
    scanf("%lf", &lengteInInches);
    printf("Dit is een lengte van %.2f cm\n", lengteInInches * 2.54);
    return 0;
}
