#include <stdio.h>

int main(void)
{
    char resultaat;
    int totaal = 0, gelijk = 0;
    printf("Geef resultaten: ");
    do
    {
        scanf("%c", &resultaat);
        if (resultaat == '1' || resultaat == '2')
        {
            totaal = totaal + 1;
        }
        else
        {
            if (resultaat == '3')
            {
                totaal = totaal + 1;
                gelijk = gelijk + 1;
            }
        }
    }
    while (resultaat != '0');
    printf("%.1f%% van de wedstrijden was een gelijkspel", gelijk * 100.0 / totaal);

    return 0;
}
