#include <stdio.h>

// © 2015 Harry Broeders

int main(void)
{
    char antw;
    do
    {
        int n;
        do
        {
            printf("Geef de breedte van de kerstboom (4 < n < 80): ");
            fflush(stdin);
        }
        while (scanf("%d", &n) != 1 || n < 4 || n > 80);

        // Print top

        for (int regel = (n - 1) / 2; regel >= 0; regel = regel - 1)
        {
            for (int spatie = 0; spatie < regel; spatie = spatie + 1)
            {
                printf(" ");
            }
            for (int plus = 0; plus < n - regel * 2; plus = plus + 1)
            {
                printf("+");
            }
            printf("\n");
        }

        // Print stam
        // De hoogte en breedte van de stam zijn afhankelijk van n

        // De formule voor de stambreedte en stamhoogte heb ik met Trial-and-error bepaald
        // Zie https://nl.wikipedia.org/wiki/Trial-and-error
        // De formule voor de stambreedte zorgt ervoor dat een stam met een even n ook een even stambreedte heeft
        // en dat een stam met een oneven n ook een oneven stambreedte heeft

        int stambreedte = (n + 17) / 20 * 2 - n % 2;
        int stamhoogte = (n + 4) / 8;

        for (int stam = 0; stam < stamhoogte; stam = stam + 1)
        {
            for (int spatie = 0; spatie < (n - stambreedte) / 2; spatie = spatie + 1)
            {
                printf(" ");
            }
            for (int ster = 0; ster < stambreedte; ster = ster + 1)
            {
                printf("*");
            }
            printf("\n");
        }

        // Print voet
        // Bomen hebben een voet van 1 + n /40 regels hoog
        // en een breedte van 3 maal de stambreedte

        int voethoogte = 1 + n / 40;
        int voetbreedte = 3 * stambreedte;

        for (int voet = 0; voet < voethoogte; voet = voet + 1)
        {
            for (int spatie = 0; spatie < (n - voetbreedte) / 2; spatie = spatie + 1)
            {
                printf(" ");
            }
            for (int ster = 0; ster < voetbreedte; ster = ster + 1)
            {
                printf("*");
            }
            printf("\n");
        }

        do
        {
            printf("Nogmaals (J / N): ");
            fflush(stdin);
        }
        while (scanf("%c", &antw) != 1 || (antw != 'J' && antw != 'j' && antw != 'N' && antw != 'n'));
    }
    while (antw != 'N' && antw != 'n');
    return 0;
}
