#include <stdio.h>

// © 2015 Harry Broeders

int main(void)
{
    int getal, even = 0, oneven = 0;

    do
    {
        printf("Geef een geheel getal groter dan of 0 om te stoppen: ");
        fflush(stdin);
        if (scanf("%d", &getal) == 1 && getal != 0)
        {
            if (getal > 0)
            {
                if (getal % 2 == 0)
                {
                    even = even + 1;
                }
                else {
                    oneven = oneven + 1;
                }
            }
            else
            {
                printf("Negatief getal genegeerd!\n");
            }
        }
    }
    while (getal != 0);

    printf("\nHet aantal even getallen is %d\n", even);
    printf("Het aantal oneven getallen is %d\n", oneven);

    return 0;
}
