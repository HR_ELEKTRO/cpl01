#include <stdio.h>

int main(void) {
	printf("Geef de orde van een magisch vierkant: ");
    int orde;
    while (scanf("%d", &orde) != 1 || orde <= 0) {
		printf("Dat was niet goed. Probeer het nog eens: ");
		fflush(stdin);
	}
	printf("De magische constante voor een magisch vierkant van %d bij %d is: %d.\n", orde, orde, orde * (orde * orde + 1) / 2);
	return 0;
}
