#include <stdio.h>

int main(void)
{
    int leesWaardeTussen(char prompt[], int min, int max);
    int begin, eind, stap, i;
    begin = leesWaardeTussen("Geef startwaarde ", -1, 99);
    eind = leesWaardeTussen("Geef eindwaarde ", begin, 101);
    stap = leesWaardeTussen("Geef stap ", 0, eind - begin + 1);
    printf("  i    i^2\n");
    printf("----------\n");
    for (i = begin; i <= eind; i = i + stap)
    {
        printf("%3d %6d\n", i, i * i);
    }
    return 0;
}

int leesWaardeTussen(char prompt[], int min, int max)
{
    int waarde;
    do
    {
        printf("%s tussen %d en %d: ", prompt, min, max);
        fflush(stdin);
    }
    while (scanf("%d", &waarde) != 1 || waarde <= min || waarde >= max);
    return waarde;
}
