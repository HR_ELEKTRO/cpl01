#include <stdio.h>
#include <stdbool.h>

bool areDiagonalsValid(int m[][9])
{
    bool checklist1[9] = {false}, checklist2[9] = {false};
    for(int rc = 0; rc < 9; rc++)
    {
        int digit1 = m[rc][rc];
        int digit2 = m[rc][8 - rc];
        if (digit1 != 0)
        {
            if (checklist1[digit1 - 1] == false)
            {
                checklist1[digit1 - 1] = true;
            }
            else
            {
                return false;
            }
        }
        if (digit2 != 0)
        {
            if (checklist2[digit2 - 1] == false)
            {
                checklist2[digit2 - 1] = true;
            }
            else
            {
                return false;
            }
        }
    }
    return true;
}

void printTestResult(int testNumber, bool testResult)
{
    if (testResult)
    {
        printf("Test %d is succesvol uitgevoerd.\n", testNumber);
    }
    else
    {
        printf("Test %d is NIET succesvol uitgevoerd!\n", testNumber);
    }
}

int main(void)
{
    int puzzle1[9][9] =
    {
        {0, 0, 0, 0, 0, 2, 0, 0, 6},
        {0, 1, 0, 9, 3, 0, 2, 0, 0},
        {0, 0, 0, 0, 7, 0, 0, 3, 0},
        {0, 4, 0, 7, 0, 0, 0, 0, 8},
        {0, 6, 5, 0, 0, 0, 7, 4, 0},
        {8, 0, 0, 0, 0, 3, 0, 5, 0},
        {0, 5, 0, 0, 2, 0, 0, 0, 0},
        {0, 0, 2, 0, 5, 4, 0, 6, 0},
        {4, 0, 0, 3, 0, 0, 0, 0, 0}
    };
    int puzzle2[9][9] =
    {
        {5, 3, 7, 4, 1, 2, 8, 9, 6},
        {6, 1, 4, 9, 3, 8, 2, 7, 5},
        {9, 2, 8, 5, 7, 6, 1, 3, 4},
        {3, 4, 1, 7, 6, 5, 9, 2, 8},
        {2, 6, 5, 8, 9, 1, 7, 4, 3},
        {8, 7, 9, 2, 4, 3, 6, 5, 1},
        {1, 5, 3, 6, 2, 9, 4, 8, 7},
        {7, 8, 2, 1, 5, 4, 3, 6, 9},
        {4, 9, 6, 3, 8, 7, 5, 1, 2}
    };
    int puzzle3[9][9] =
    {
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0}
    };
    int puzzle4[9][9] =
    {
        {0, 0, 0, 0, 0, 0, 0, 0, 1},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 0, 0, 0, 0, 0, 0, 0, 0}
    };
    int puzzle5[9][9] =
    {
        {9, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 9}
    };
    int puzzle6[9][9] =
    {
        {1, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 2, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 3, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 4, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 5, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 6, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 7, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 8, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 8}
    };

    printTestResult(1, areDiagonalsValid(puzzle1) == true);
    printTestResult(2, areDiagonalsValid(puzzle2) == true);
    printTestResult(3, areDiagonalsValid(puzzle3) == true);
    printTestResult(4, areDiagonalsValid(puzzle4) == false);
    printTestResult(5, areDiagonalsValid(puzzle5) == false);
    printTestResult(6, areDiagonalsValid(puzzle6) == false);

    return 0;
}
