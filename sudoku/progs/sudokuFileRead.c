#include <stdio.h>
#include <stdbool.h>

bool isEqual(int m1[][9], int m2[][9])
{
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            if (m1[r][c] != m2[r][c])
            {
                return false;
            }
        }
    }
    return true;
}

bool readPuzzle(FILE* fp, int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            if (fscanf(fp, "%d", &m[r][c]) != 1)
            {
                return false;
            }
        }
    }
    return true;
}

void printTestResult(int testNumber, int testResult)
{
    if (testResult)
    {
        printf("Test %d is succesvol uitgevoerd.\n", testNumber);
    }
    else
    {
        printf("Test %d is NIET succesvol uitgevoerd!\n", testNumber);
    }
}

int main(int argc, char *argv[])
{
    int puzzle1[9][9] =
    {
        {8, 6, 0, 0, 2, 0, 0, 0, 0},
        {0, 0, 0, 7, 0, 0, 0, 5, 9},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 6, 0, 8, 0, 0},
        {0, 4, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 5, 3, 0, 0, 0, 0, 7},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 2, 0, 0, 0, 0, 6, 0, 0},
        {0, 0, 7, 5, 0, 9, 0, 0, 0}
    };
    int puzzle2[9][9];

    FILE * file = fopen("sudoku_2.txt", "r");
    if (file == NULL)
    {
        perror("Error sudoku_2.txt");
        return 1;
    }
    printTestResult(1, readPuzzle(file, puzzle2) == true && isEqual(puzzle1, puzzle2) == true);

    return 0;
}
