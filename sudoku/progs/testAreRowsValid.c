#include <stdio.h>
#include <stdbool.h>

bool areRowsValid(int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        bool checklist[9] = {false};
        for (int c = 0; c < 9; c++)
        {
            int digit = m[r][c];
            if (digit != 0)
            {
                if (checklist[digit - 1])
                {
                    return false;
                }
                else
                {
                    checklist[digit - 1] = true;
                }
            }
        }
    }
    return true;
}

void printTestResult(int testNumber, bool testResult)
{
    if (testResult)
    {
        printf("Test %d is succesvol uitgevoerd.\n", testNumber);
    }
    else
    {
        printf("Test %d is NIET succesvol uitgevoerd!\n", testNumber);
    }
}

int main(void)
{
    int puzzle1[9][9] =
    {
        {8, 6, 0, 0, 2, 0, 0, 0, 0},
        {0, 0, 0, 7, 0, 0, 0, 5, 9},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 6, 0, 8, 0, 0},
        {0, 4, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 5, 3, 0, 0, 0, 0, 7},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 2, 0, 0, 0, 0, 6, 0, 0},
        {0, 0, 7, 5, 0, 9, 0, 0, 0}
    };
    int puzzle2[9][9] =
    {
        {8, 6, 3, 9, 2, 5, 7, 4, 1},
        {4, 1, 2, 7, 8, 6, 3, 5, 9},
        {7, 5, 9, 4, 1, 3, 2, 8, 6},
        {9, 7, 1, 2, 6, 4, 8, 3, 5},
        {3, 4, 6, 8, 5, 7, 9, 1, 2},
        {2, 8, 5, 3, 9, 1, 4, 6, 7},
        {1, 9, 8, 6, 3, 2, 5, 7, 4},
        {5, 2, 4, 1, 7, 8, 6, 9, 3},
        {6, 3, 7, 5, 4, 9, 1, 2, 8}
    };
    int puzzle3[9][9] =
    {
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0}
    };
    int puzzle4[9][9] =
    {
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 0, 0, 0, 0, 0, 0, 0, 1},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0}
    };
    int puzzle5[9][9] =
    {
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {9, 0, 0, 0, 0, 0, 0, 0, 9},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0}
    };
    int puzzle6[9][9] =
    {
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 2, 3, 4, 5, 6, 7, 8, 8},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0}
    };

    printTestResult(1, areRowsValid(puzzle1) == true);
    printTestResult(2, areRowsValid(puzzle2) == true);
    printTestResult(3, areRowsValid(puzzle3) == true);
    printTestResult(4, areRowsValid(puzzle4) == false);
    printTestResult(5, areRowsValid(puzzle5) == false);
    printTestResult(6, areRowsValid(puzzle6) == false);

    return 0;
}
