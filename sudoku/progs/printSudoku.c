#include <stdio.h>

void printPuzzle(int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        if (r % 3 == 0)
        {
            printf(" ------- ------- -------\n");
        }
        for (int c = 0; c < 9; c++)
        {
            if (c % 3 == 0)
            {
                printf("| ");
            }
            int digit = m[r][c];
            if (digit == 0)
            {
                printf("  ");
            }
            else
            {
                printf("%d ", m[r][c]);
            }
        }
        printf("|\n");
    }
    printf(" ------- ------- -------\n");
}

int main(void)
{
    int puzzle1[9][9] =
    {
        {8, 6, 0, 0, 2, 0, 0, 0, 0},
        {0, 0, 0, 7, 0, 0, 0, 5, 9},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 6, 0, 8, 0, 0},
        {0, 4, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 5, 3, 0, 0, 0, 0, 7},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 2, 0, 0, 0, 0, 6, 0, 0},
        {0, 0, 7, 5, 0, 9, 0, 0, 0}
    };

    printf("Puzzel 1:\n");
    printPuzzle(puzzle1);

    return 0;
}
