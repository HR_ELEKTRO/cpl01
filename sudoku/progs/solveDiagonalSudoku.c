#include <stdio.h>
#include <stdbool.h>

bool areRowsValid(int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        bool checklist[9] = {false};
        for (int c = 0; c < 9; c++)
        {
            int digit = m[r][c];
            if (digit != 0)
            {
                if (checklist[digit - 1])
                {
                    return false;
                }
                else
                {
                    checklist[digit - 1] = true;
                }
            }
        }
    }
    return true;
}

bool areColumnsValid(int m[][9])
{
    for (int c = 0; c < 9; c++)
    {
        bool checklist[9] = {false};
        for (int r = 0; r < 9; r++)
        {
            int digit = m[r][c];
            if (digit != 0)
            {
                if (checklist[digit - 1])
                {
                    return false;
                }
                else
                {
                    checklist[digit - 1] = true;
                }
            }
        }
    }
    return true;
}

bool areBlocksValid(int m[][9])
{
    for (int rb = 0; rb < 9; rb += 3)
    {
        for (int cb = 0; cb < 9; cb += 3)
        {
            bool checklist[9] = {false};
            for (int r = rb; r < rb + 3; r++)
            {
                for (int c = cb; c < cb + 3; c++)
                {
                    int digit = m[r][c];
                    if (digit != 0)
                    {
                        if (checklist[digit - 1])
                        {
                            return false;
                        }
                        else
                        {
                            checklist[digit - 1] = true;
                        }
                    }
                }
            }
        }
    }
    return true;
}

bool areDiagonalsValid(int m[][9])
{
    bool checklist1[9] = {false}, checklist2[9] = {false};
    for(int rc = 0; rc < 9; rc++)
    {
        int digit1 = m[rc][rc];
        int digit2 = m[rc][8 - rc];
        if (digit1 != 0)
        {
            if (checklist1[digit1 - 1] == false)
            {
                checklist1[digit1 - 1] = true;
            }
            else
            {
                return false;
            }
        }
        if (digit2 != 0)
        {
            if (checklist2[digit2 - 1] == false)
            {
                checklist2[digit2 - 1] = true;
            }
            else
            {
                return false;
            }
        }
    }
    return true;
}

bool isValid(int m[][9])
{
    return areRowsValid(m) && areColumnsValid(m) && areBlocksValid(m) && areDiagonalsValid(m);
}

bool solveLow(int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            if (m[r][c] == 0)
            {
                for (int digit = 1; digit <= 9; digit++)
                {
                    m[r][c] = digit;
                    if (isValid(m) && solveLow(m))
                    {
                        return true;
                    }
                    m[r][c] = 0;
                }
                return false;
            }
        }
    }
    return isValid(m);
}

bool solveHigh(int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            if (m[r][c] == 0)
            {
                for (int digit = 9; digit >= 1; digit--)
                {
                    m[r][c] = digit;
                    if (isValid(m) && solveHigh(m))
                    {
                        return true;
                    }
                    m[r][c] = 0;
                }
                return false;
            }
        }
    }
    return isValid(m);
}

void copy(int to[][9], int from[][9])
{
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            to[r][c] = from[r][c];
        }
    }
}

bool isEqual(int m1[][9], int m2[][9])
{
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            if (m1[r][c] != m2[r][c])
            {
                return false;
            }
        }
    }
    return true;
}

bool solve(int puzzle[][9])
{
    int result1[9][9], result2[9][9];
    copy(result1, puzzle);
    if (solveLow(result1))
    {
        copy(result2, puzzle);
        if (solveHigh(result2) && isEqual(result1, result2))
        {
            copy(puzzle, result1);
            return true;
        }
    }
    return false;
}

bool readPuzzle(FILE* fp, int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            if (fscanf(fp, "%d", &m[r][c]) != 1)
            {
                return false;
            }
        }
    }
    return true;
}

void printPuzzle(int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        if (r % 3 == 0)
        {
            printf(" ------- ------- -------\n");
        }
        for (int c = 0; c < 9; c++)
        {
            if (c % 3 == 0)
            {
                printf("| ");
            }
            int digit = m[r][c];
            if (digit == 0)
            {
                printf("  ");
            }
            else
            {
                printf("%d ", m[r][c]);
            }
        }
        printf("|\n");
    }
    printf(" ------- ------- -------\n");
}

int main(int argc, char *argv[])
{
    if (argc > 2)
    {
        fprintf(stderr, "Gebruik: %s [<input.txt>]\n", argv[0]);
        return 1;
    }
    char* inputFilename;
    if (argc > 1)
    {
        inputFilename = argv[1];
    }
    else
    {
        char filename[200];
        printf("Geef filenaam: ");
        scanf("%199s", filename);
        inputFilename = filename;
    }
    FILE* inFile = fopen(inputFilename, "r");
    if (inFile == NULL)
    {
        fprintf(stderr, "Kan bestand %s niet openen voor lezen!\n", inputFilename);
        perror("Error");
        return 2;
    }
    int puzzle[9][9];
    if (!readPuzzle(inFile, puzzle))
    {
        fprintf(stderr, "Fout tijdens lezen van bestand %s!", inputFilename);
        perror("Error");
        return 3;
    }
    else
    {
        printPuzzle(puzzle);
        printf("Oplossing:\n");
        if (solve(puzzle))
        {
            printPuzzle(puzzle);
        }
        else
        {
            printf("Is niet mogelijk!\n");
        }
    }
    return 0;
}
