#include <stdio.h>
#include <stdbool.h>

bool readPuzzle(FILE* fp, int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            if (fscanf(fp, "%d", &m[r][c]) != 1)
            {
                return false;
            }
        }
    }
    return true;
}

void printPuzzle(int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        if (r % 3 == 0)
        {
            printf(" ------- ------- -------\n");
        }
        for (int c = 0; c < 9; c++)
        {
            if (c % 3 == 0)
            {
                printf("| ");
            }
            int digit = m[r][c];
            if (digit == 0)
            {
                printf("  ");
            }
            else
            {
                printf("%d ", m[r][c]);
            }
        }
        printf("|\n");
    }
    printf(" ------- ------- -------\n");
}

int main(int argc, char *argv[])
{
    if (argc > 2)
    {
        fprintf(stderr, "Gebruik: %s [<input.txt>]\n", argv[0]);
        return 1;
    }
    char* inputFilename;
    if (argc > 1)
    {
        inputFilename = argv[1];
    }
    else
    {
        char filename[200];
        printf("Geef filenaam: ");
        scanf("%199s", filename);
        inputFilename = filename;
    }
    FILE* inFile = fopen(inputFilename, "r");
    if (inFile == NULL)
    {
        fprintf(stderr, "Kan bestand %s niet openen voor lezen!\n", inputFilename);
        perror("Error");
        return 2;
    }
    int puzzle[9][9];
    if (!readPuzzle(inFile, puzzle))
    {
        fprintf(stderr, "Fout tijdens lezen van bestand %s!", inputFilename);
        perror("Error");
        return 3;
    }
    else
    {
        printPuzzle(puzzle);
    }
    return 0;
}
