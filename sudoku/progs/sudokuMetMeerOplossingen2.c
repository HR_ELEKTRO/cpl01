#include <stdio.h>
#include <stdbool.h>

bool areRowsValid(int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        bool checklist[9] = {false};
        for (int c = 0; c < 9; c++)
        {
            int digit = m[r][c];
            if (digit != 0)
            {
                if (checklist[digit - 1])
                {
                    return false;
                }
                else
                {
                    checklist[digit - 1] = true;
                }
            }
        }
    }
    return true;
}

bool areColumnsValid(int m[][9])
{
    for (int c = 0; c < 9; c++)
    {
        bool checklist[9] = {false};
        for (int r = 0; r < 9; r++)
        {
            int digit = m[r][c];
            if (digit != 0)
            {
                if (checklist[digit - 1])
                {
                    return false;
                }
                else
                {
                    checklist[digit - 1] = true;
                }
            }
        }
    }
    return true;
}

bool areBlocksValid(int m[][9])
{
    for (int rb = 0; rb < 9; rb += 3)
    {
        for (int cb = 0; cb < 9; cb += 3)
        {
            bool checklist[9] = {false};
            for (int r = rb; r < rb + 3; r++)
            {
                for (int c = cb; c < cb + 3; c++)
                {
                    int digit = m[r][c];
                    if (digit != 0)
                    {
                        if (checklist[digit - 1])
                        {
                            return false;
                        }
                        else
                        {
                            checklist[digit - 1] = true;
                        }
                    }
                }
            }
        }
    }
    return true;
}

bool isValid(int m[][9])
{
    return areRowsValid(m) && areColumnsValid(m) && areBlocksValid(m);
}

bool solveHigh(int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            if (m[r][c] == 0)
            {
                for (int digit = 9; digit >= 1; digit--)
                {
                    m[r][c] = digit;
                    if (isValid(m) && solveHigh(m))
                    {
                        return true;
                    }
                    m[r][c] = 0;
                }
                return false;
            }
        }
    }
    return isValid(m);
}

void printPuzzle(int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        if (r % 3 == 0)
        {
            printf(" ------- ------- -------\n");
        }
        for (int c = 0; c < 9; c++)
        {
            if (c % 3 == 0)
            {
                printf("| ");
            }
            int digit = m[r][c];
            if (digit == 0)
            {
                printf("  ");
            }
            else
            {
                printf("%d ", m[r][c]);
            }
        }
        printf("|\n");
    }
    printf(" ------- ------- -------\n");
}

bool isEqual(int m1[][9], int m2[][9])
{
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            if (m1[r][c] != m2[r][c])
            {
                return false;
            }
        }
    }
    return true;
}

void printTestResult(int testNumber, int testResult)
{
    if (testResult)
    {
        printf("Test %d is succesvol uitgevoerd.\n", testNumber);
    }
    else
    {
        printf("Test %d is NIET succesvol uitgevoerd!\n", testNumber);
    }
}

int main(void)
{
    int puzzle1[9][9] =
    {
        {8, 6, 0, 0, 2, 0, 0, 0, 0},
        {0, 0, 0, 7, 0, 0, 0, 5, 9},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 6, 0, 8, 0, 0},
        {0, 4, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 5, 3, 0, 0, 0, 0, 7},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 2, 0, 0, 0, 0, 6, 0, 0},
        {0, 0, 7, 5, 0, 9, 0, 0, 0}
    };
    int solution1[9][9] =
    {
        {8, 6, 3, 9, 2, 5, 7, 4, 1},
        {4, 1, 2, 7, 8, 6, 3, 5, 9},
        {7, 5, 9, 4, 1, 3, 2, 8, 6},
        {9, 7, 1, 2, 6, 4, 8, 3, 5},
        {3, 4, 6, 8, 5, 7, 9, 1, 2},
        {2, 8, 5, 3, 9, 1, 4, 6, 7},
        {1, 9, 8, 6, 3, 2, 5, 7, 4},
        {5, 2, 4, 1, 7, 8, 6, 9, 3},
        {6, 3, 7, 5, 4, 9, 1, 2, 8}
    };
    int puzzle2[9][9] =
    {
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0}
    };

    printf("Puzzel 1:\n");
    printPuzzle(puzzle1);
    printTestResult(1, solveHigh(puzzle1) == true && isEqual(puzzle1, solution1) == true);
    printf("Oplossing puzzel 1:\n");
    printPuzzle(puzzle1);

    printf("\nPuzzel 2:\n");
    printPuzzle(puzzle2);
    printTestResult(2, solveHigh(puzzle2) == false);
    printf("Oplossing puzzel 2:\n");
    printPuzzle(puzzle2);

    return 0;
}
