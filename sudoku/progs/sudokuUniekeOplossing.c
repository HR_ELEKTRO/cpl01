#include <stdio.h>
#include <stdbool.h>

bool areRowsValid(int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        bool checklist[9] = {false};
        for (int c = 0; c < 9; c++)
        {
            int digit = m[r][c];
            if (digit != 0)
            {
                if (checklist[digit - 1])
                {
                    return false;
                }
                else
                {
                    checklist[digit - 1] = true;
                }
            }
        }
    }
    return true;
}

bool areColumnsValid(int m[][9])
{
    for (int c = 0; c < 9; c++)
    {
        bool checklist[9] = {false};
        for (int r = 0; r < 9; r++)
        {
            int digit = m[r][c];
            if (digit != 0)
            {
                if (checklist[digit - 1])
                {
                    return false;
                }
                else
                {
                    checklist[digit - 1] = true;
                }
            }
        }
    }
    return true;
}

bool areBlocksValid(int m[][9])
{
    for (int rb = 0; rb < 9; rb += 3)
    {
        for (int cb = 0; cb < 9; cb += 3)
        {
            bool checklist[9] = {false};
            for (int r = rb; r < rb + 3; r++)
            {
                for (int c = cb; c < cb + 3; c++)
                {
                    int digit = m[r][c];
                    if (digit != 0)
                    {
                        if (checklist[digit - 1])
                        {
                            return false;
                        }
                        else
                        {
                            checklist[digit - 1] = true;
                        }
                    }
                }
            }
        }
    }
    return true;
}

bool isValid(int m[][9])
{
    return areRowsValid(m) && areColumnsValid(m) && areBlocksValid(m);
}

bool solveLow(int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            if (m[r][c] == 0)
            {
                for (int digit = 1; digit <= 9; digit++)
                {
                    m[r][c] = digit;
                    if (isValid(m) && solveLow(m))
                    {
                        return true;
                    }
                    m[r][c] = 0;
                }
                return false;
            }
        }
    }
    return isValid(m);
}

bool solveHigh(int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            if (m[r][c] == 0)
            {
                for (int digit = 9; digit >= 1; digit--)
                {
                    m[r][c] = digit;
                    if (isValid(m) && solveHigh(m))
                    {
                        return true;
                    }
                    m[r][c] = 0;
                }
                return false;
            }
        }
    }
    return isValid(m);
}

void copy(int to[][9], int from[][9])
{
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            to[r][c] = from[r][c];
        }
    }
}

bool isEqual(int m1[][9], int m2[][9])
{
    for (int r = 0; r < 9; r++)
    {
        for (int c = 0; c < 9; c++)
        {
            if (m1[r][c] != m2[r][c])
            {
                return false;
            }
        }
    }
    return true;
}

bool solve(int puzzle[][9])
{
    int result1[9][9], result2[9][9];
    copy(result1, puzzle);
    if (solveLow(result1))
    {
        copy(result2, puzzle);
        if (solveHigh(result2) && isEqual(result1, result2))
        {
            copy(puzzle, result1);
            return true;
        }
    }
    return false;
}

void printPuzzle(int m[][9])
{
    for (int r = 0; r < 9; r++)
    {
        if (r % 3 == 0)
        {
            printf(" ------- ------- -------\n");
        }
        for (int c = 0; c < 9; c++)
        {
            if (c % 3 == 0)
            {
                printf("| ");
            }
            int digit = m[r][c];
            if (digit == 0)
            {
                printf("  ");
            }
            else
            {
                printf("%d ", m[r][c]);
            }
        }
        printf("|\n");
    }
    printf(" ------- ------- -------\n");
}

void printTestResult(int testNumber, int testResult)
{
    if (testResult)
    {
        printf("Test %d is succesvol uitgevoerd.\n", testNumber);
    }
    else
    {
        printf("Test %d is NIET succesvol uitgevoerd!\n", testNumber);
    }
}

int main(void)
{
    int puzzle1[9][9] =
    {
        {8, 6, 0, 0, 2, 0, 0, 0, 0},
        {0, 0, 0, 7, 0, 0, 0, 5, 9},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 6, 0, 8, 0, 0},
        {0, 4, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 5, 3, 0, 0, 0, 0, 7},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 2, 0, 0, 0, 0, 6, 0, 0},
        {0, 0, 7, 5, 0, 9, 0, 0, 0}
    };

    int puzzle2[9][9] =
    {
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0}
    };

    int puzzle[9][9];
    copy(puzzle, puzzle1);
    printTestResult(1, solve(puzzle) == true);
    copy(puzzle, puzzle1);
    printf("solveLow(puzzle1):\n");
    solveLow(puzzle);
    printPuzzle(puzzle);

    copy(puzzle, puzzle1);
    solveHigh(puzzle);
    printf("solveHigh(puzzle1):\n");
    printPuzzle(puzzle);

    copy(puzzle, puzzle0);
    printTestResult(2, solve(puzzle) == false);
    copy(puzzle, puzzle0);
    solveLow(puzzle);
    printf("solveLow(puzzle0):\n");
    printPuzzle(puzzle);

    copy(puzzle, puzzle0);
    solveHigh(puzzle);
    printf("solveHigh(puzzle0):\n");
    printPuzzle(puzzle);

    return 0;
}
