#include <stdio.h>

int main(void)
{
    float fahrenheit;
    float celcius;
    printf("     F      C\n");
    for (fahrenheit = 32.0; fahrenheit <= 33.0; fahrenheit = fahrenheit + 0.1)
    {
        celcius=(fahrenheit - 32.0) / 1.8;
        printf("%6.2f %6.2f\n", fahrenheit, celcius);
    }
    return 0;
}
