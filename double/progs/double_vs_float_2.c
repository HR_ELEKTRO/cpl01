#include <stdio.h>

int main(void)
{
    float f = 102.0 / 10.0;
    double d = 102.0 / 10.0;
    printf("f = %.20f\n", f);
    printf("d = %.20f\n", d);
    return 0;
}
