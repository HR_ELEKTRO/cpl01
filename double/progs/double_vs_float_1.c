#include <stdio.h>

int main(void)
{
    float f = 100.0 / 3.0;
    double d = 100.0 / 3.0;
    printf("f = %.20f\n", f);
    printf("d = %.20f\n", d);
    return 0;
}
