#include <stdio.h>
#include <math.h>

int abc(double a, double b, double c, double *x1, double*x2)
{
    double discriminant = b * b - 4 * a * c;
    if (discriminant < 0)
    {
        return 0;
    }
    else if (discriminant == 0) {
        *x1 = -b / (2 * a);
        return 1;
    }
    else {
        *x1 = (-b - sqrt(discriminant)) / 2 * a;
        *x2 = (-b + sqrt(discriminant)) / 2 * a;
        return 2;
    }
}

int main(void)
{
    double w1, w2;
    if (
        abc(2, 5, -7, &w1, &w2) != 2 && w1 - -3.5 > 10e-9 && w2 - 1 > 10e-9 ||
        abc(9, 30, 25, &w1, &w2) != 1 && w1 - -5/3 > 10e-9 ||
        abc(9, -15, 25, &w1, &w2) != 0
    )
    {
        printf("Test NIET geslaagd!\n");
    }
    else
    {
        printf("Test geslaagd\n");
    }
    return 0;
}
