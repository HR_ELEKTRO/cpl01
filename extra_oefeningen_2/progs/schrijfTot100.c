#include <stdio.h>

void schrijfTot10(int getalTot10)
{
    switch (getalTot10)
    {
    case 0:
        printf("nul");
        break;
    case 1:
        printf("een");
        break;
    case 2:
        printf("twee");
        break;
    case 3:
        printf("drie");
        break;
    case 4:
        printf("vier");
        break;
    case 5:
        printf("vijf");
        break;
    case 6:
        printf("zes");
        break;
    case 7:
        printf("zeven");
        break;
    case 8:
        printf("acht");
        break;
    case 9:
        printf("negen");
        break;
    default:
        printf("Functie schrijfTot10 aangeroepen met ongeldig argument (%d)\n", getalTot10);
        break;
    }
}

void schrijfTot100(int getalTot100)
{
    if (getalTot100 >= 0 && getalTot100 <= 99)
    {
        if (getalTot100 < 10)
        {
            schrijfTot10(getalTot100);

        }
        else if (getalTot100 < 15)
        {
            switch (getalTot100)
            {
            case 10:
                printf("tien");
                break;
            case 11:
                printf("elf");
                break;
            case 12:
                printf("twaalf");
                break;
            case 13:
                printf("dertien");
                break;
            case 14:
                printf("veertien");
                break;
            }
        }
        else
        {
            int eenheden = getalTot100 % 10;
            int tientallen = getalTot100 / 10;
            if (eenheden > 0)
            {
                schrijfTot10(eenheden);
                if (tientallen > 1)
                {
                    printf("en");
                }
            }
            switch(tientallen)
            {
            case 1:
                printf("tien");
                break;
            case 2:
                printf("twin");
                break;
            case 3:
                printf("der");
                break;
            case 4:
                printf("veer");
                break;
            case 8:
                printf("tach");
                break;
            default:
                schrijfTot10(tientallen);
            }
            if (tientallen > 1)
            {
                printf("tig");
            }
        }
    }
    else
    {
        printf("Functie schrijfTot100 aangeroepen met ongeldig argument (%d)\n", getalTot100);
    }
}

int main(void)
{
    for (int getal = -1; getal < 101; getal++)
    {
        schrijfTot100(getal);
        printf("\n");
    }
    return 0;
}
