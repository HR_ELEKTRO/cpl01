#include <stdio.h>

void print_binary(int getal)
{
    if (getal >= 0 && getal <= 255)
    {
        for (int bitwaarde = 128; bitwaarde > 0; bitwaarde = bitwaarde / 2)
        {
            if (getal >= bitwaarde)
            {
                printf("1");
                getal = getal - bitwaarde;
            }
            else
            {
                printf("0");
            }
        }
    }
    else
    {
        printf("Functie print_binary aangeroepen met ongeldig argument (%d)\n", getal);
    }
}

int main(void)
{
    for (int rij = 0; rij < 32; rij++)
    {
        for (int kolom = 0; kolom < 8; kolom++)
        {
            print_binary(rij * 8 + kolom);
            printf(" ");
        }
        printf("\n");
    }
    return 0;
}
