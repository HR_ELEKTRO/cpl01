#include <stdio.h>

int isPerfect(int getal)
{
    return getal == 6 | getal == 28 | getal == 496 | getal == 8128 | getal == 33550336;
}

int main(void)
{
    // testprogramma voor isPerfect
    if (
        isPerfect(0) == 0 &&
        isPerfect(1) == 0 &&
        isPerfect(5) == 0 &&
        isPerfect(6) == 1 &&
        isPerfect(7) == 0 &&
        isPerfect(28) == 1 &&
        isPerfect(101) == 0 &&
        isPerfect(496) == 1 &&
        isPerfect(8128) == 1 &&
        isPerfect(33550336) == 1 &&
        isPerfect(-33550336) == 0
    )
    {
        printf("Test geslaagd.\n");
    }
    else
    {
        printf("Test NIET geslaagd!\n");
    }
}
