#include <stdio.h>

void wissel(int *p, int *q)
{
    int hulpje = *p;
    *p = *q;
    *q = hulpje;
}

// Deze oplossing gebruikt de parameters a en n waardoor geen lokale variabelen nodig zijn
void reverse(int a[], int n)
{
    while (a < a + --n)
    {
        wissel(a++, a + n--);
    }
}

int main(void)
{
    int a[] = {1, 2, 3, 4};
    reverse(a, sizeof a / sizeof a[0]);
    if (a[0] != 4 || a[1] != 3 || a[2] != 2 || a[3] != 1)
    {
        printf("Test1 NIET geslaagd!\n");
    }
    else
    {
        reverse(a, 1);
        if (a[0] != 4 || a[1] != 3 || a[2] != 2 || a[3] != 1)
        {
            printf("Test2 NIET geslaagd!\n");
        }
        else
        {
            reverse(a, 3);
            if (a[0] != 2 || a[1] != 3 || a[2] != 4 || a[3] != 1)
            {
                printf("Test3 NIET geslaagd!\n");
            }
            else
            {
                printf("Alle testen geslaagd.\n");
            }
        }
    }
    return 0;
}
