#include <stdio.h>

int checkGetallen(int vierkant[8][8])
{
    int markeer[8*8] = {0};
    int rij, kolom;
    for (rij = 0; rij < 8; rij = rij + 1)
    {
        for (kolom = 1; kolom < 8; kolom = kolom + 1)
        {
            int getal = vierkant[rij][kolom];
            if (getal < 1 || getal > 8 * 8 || markeer[getal - 1] != 0)
            {
                return 0;
            }
            markeer[getal - 1] = 1;
        }
    }
    return 1;
}

int checkSomRijen(int vierkant[8][8])
{
    int rij;
    for (rij = 0; rij < 8; rij = rij + 1)
    {
        int som = vierkant[rij][0];
        int kolom;
        for (kolom = 1; kolom < 8; kolom = kolom + 1)
        {
            som = som + vierkant[rij][kolom];
        }
        if (som != 260)
        {
            return 0;
        }
    }
    return 1;
}

int checkSomKolommen(int vierkant[8][8])
{
    int kolom;
    for (kolom = 0; kolom < 8; kolom = kolom + 1)
    {
        int som = vierkant[0][kolom];
        int rij;
        for (rij = 1; rij < 8; rij = rij + 1)
        {
            som = som + vierkant[rij][kolom];
        }
        if (som != 260)
        {
            return 0;
        }
    }
    return 1;
}

int checkSomDiagonalen(int vierkant[8][8])
{
    int somDiagonaal1 = vierkant[0][0];
    int somDiagonaal2 = vierkant[0][8 - 1];
    int i;
    for (i = 1; i < 8; i = i + 1)
    {
        somDiagonaal1 = somDiagonaal1 + vierkant[i][i];
        somDiagonaal2 = somDiagonaal2 + vierkant[i][8 - 1 - i];
    }
    return somDiagonaal1 == 260 && somDiagonaal2 == 260;
}

int isMagisch(int vierkant[8][8])
{
    return checkGetallen(vierkant) && checkSomRijen(vierkant) && checkSomKolommen(vierkant) && checkSomDiagonalen(vierkant);
}

int main(void)
{
    int test1[8][8] =
    {
        { 8, 58, 59,  5,  4, 62, 63,  1},
        {49, 15, 14, 52, 53, 11, 10, 56},
        {41, 23, 22, 44, 45, 19, 18, 48},
        {32, 34, 35, 29, 28, 38, 39, 25},
        {40, 26, 27, 37, 36, 30, 31, 33},
        {17, 47, 46, 20, 21, 43, 42, 24},
        { 9, 55, 54, 12, 13, 51, 50, 16},
        {64,  2,  3, 61, 60,  6,  7, 57}
    };

    int test2[8][8] =
    {
        { 8, 58, 59,  5,  4, 62, 63,  1},
        {49, 15, 14, 52, 53, 11, 10, 56},
        {41, 23, 22, 44, 45, 19, 18, 48},
        {32, 34, 35, 29, 28, 38, 39, 25},
        {40, 26, 27, 37, 36, 30, 31, 33},
        {17, 47, 46, 20, 21, 43, 42, 24},
        { 9, 55, 54, 12, 13, 51, 50, 16},
        {63,  2,  3, 61, 60,  6,  7, 57}
    };

    if (isMagisch(test1) == 1 && isMagisch(test2) == 0)
    {
        printf("Test geslaagd.");
    }
    else
    {
        printf("Test NIET geslaagd!");
    }

    return 0;
}
