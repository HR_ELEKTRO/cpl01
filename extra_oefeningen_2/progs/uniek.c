#include <stdio.h>

int uniek(int a[], int aantal_a)
{
    // De unieke getallen worden naar voren gekopieerd.
    int uniek_i = 0;
    for (int i = 0; i < aantal_a; i++)
    {
        // check of a[i] (tot nog toe) uniek is
        int isUniek = 1;
        for (int j = 0; j < i; j++)
        {
            if (a[j] == a[i])
            {
                isUniek = 0;
                break;
            }
        }
        if (isUniek)
        {
            a[uniek_i++] = a[i];
        }
    }
    return uniek_i;
}

int main(void)
{
    int rij1[] = { 23, 3, 56, 3, 23, 5, 12, 6, 3, 18, 7, 10, 56, 11 };
    int rij2[] = { 3, 3, 3, 3 };
    int aantal_uniek = uniek(rij1, sizeof rij1 / sizeof rij1[0]);
    for (int i = 0; i < aantal_uniek; i++)
    {
        printf("%d ", rij1[i]);
    }
    printf("\n");
    aantal_uniek = uniek(rij2, sizeof rij2 / sizeof rij2[0]);
    for (int i = 0; i < aantal_uniek; i++)
    {
        printf("%d ", rij2[i]);
    }
    printf("\n");

    // flauw maar moet wel werken
    aantal_uniek = uniek(rij2, 0);
    for (int i = 0; i < aantal_uniek; i++)
    {
        printf("%d ", rij2[i]);
    }
    printf("\n");

    return 0;
}
