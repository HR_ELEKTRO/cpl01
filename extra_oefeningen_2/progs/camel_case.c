#include <stdio.h>
#include <ctype.h>

void toCamelCase(char naam[])
{
    int i = 0, j = 0;
    while (naam[j] != '\0')
    {
        if (naam[i] == '_')
        {
            i = i + 1;
            naam[j] = toupper(naam[i]);
        }
        else
        {
            naam[j] = naam[i];
        }
        i = i + 1;
        j = j + 1;
    }
}

int main(void)
{
    char voorbeeld1[] = "dit_is_een_voorbeeld";
    char voorbeeld2[] = "en_dit_ook";
    char voorbeeld3[] = "all_4_one";
    char voorbeeld4[] = "laatste_";
    char voorbeeld5[] = "_eerste";
    toCamelCase(voorbeeld1);
    toCamelCase(voorbeeld2);
    toCamelCase(voorbeeld3);
    toCamelCase(voorbeeld4);
    toCamelCase(voorbeeld5);
    printf("%s %s %s %s %s\n", voorbeeld1, voorbeeld2, voorbeeld3, voorbeeld4, voorbeeld5);
    return 0;
}

