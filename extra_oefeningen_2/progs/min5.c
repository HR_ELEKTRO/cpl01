#include <stdio.h>

int min2(int i1, int i2)
{
    if (i1 < i2)
    {
        return i1;
    }
    else
    {
        return i2;
    }
}

int min5(int i1, int i2, int i3, int i4, int i5)
{
    return min2(min2(min2(i1, i2), min2(i3, i4)), i5);
}

int main(void)
{
    // testprogramma voor min5
    if (
        min5(10, 11, 12, 13, 14) == 10 &&
        min5(11, 10, 12, 13, 14) == 10 &&
        min5(11, 12, 10, 13, 14) == 10 &&
        min5(11, 12, 13, 10, 14) == 10 &&
        min5(11, 12, 13, 14, 10) == 10 &&
        min5(10, 10, 10, 10, 10) == 10 &&
        min5(10, 10, -10, 10, 10) == -10
    )
    {
        printf("Test geslaagd.\n");
    }
    else
    {
        printf("Test NIET geslaagd!\n");
        printf("min5(10, 11, 12, 13, 14) => %d\n", min5(10, 11, 12, 13, 14));
        printf("min5(11, 10, 12, 13, 14) => %d\n", min5(11, 10, 12, 13, 14));
        printf("min5(11, 12, 10, 13, 14) => %d\n", min5(11, 12, 10, 13, 14));
        printf("min5(11, 12, 13, 10, 14) => %d\n", min5(11, 12, 13, 10, 14));
        printf("min5(11, 12, 13, 14, 10) => %d\n", min5(11, 12, 13, 14, 10));
        printf("min5(10, 10, 10, 10, 10) => %d\n", min5(10, 10, 10, 10, 10));
        printf("min5(10, 10, -10, 10, 10) => %d\n", min5(10, 10, -10, 10, 10));
    }
}
