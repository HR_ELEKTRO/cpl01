#include <stdio.h>

void schrijfTot10(int getalTot10)
{
    switch (getalTot10)
    {
    case 0:
        printf("nul");
        break;
    case 1:
        printf("een");
        break;
    case 2:
        printf("twee");
        break;
    case 3:
        printf("drie");
        break;
    case 4:
        printf("vier");
        break;
    case 5:
        printf("vijf");
        break;
    case 6:
        printf("zes");
        break;
    case 7:
        printf("zeven");
        break;
    case 8:
        printf("acht");
        break;
    case 9:
        printf("negen");
        break;
    default:
        printf("Functie schrijfTot10 aangeroepen met ongeldig argument (%d)\n", getalTot10);
        break;
    }
}

int main(void)
{
    for (int getal = -1; getal < 11; getal++)
    {
        schrijfTot10(getal);
        printf("\n");
    }
    return 0;
}
