#include <stdio.h>
#include <stdlib.h>

// Zie http://en.cppreference.com/w/c/algorithm/qsort voor gebruik qsort

int compare_ints(const void* a, const void* b)
{
    int arg1 = *(const int*)a;
    int arg2 = *(const int*)b;

    if (arg1 < arg2) return -1;
    if (arg1 > arg2) return 1;
    return 0;
}

int uniek(int a[], int aantal_a)
{
    // speciaal geval
    if (aantal_a <= 0) {
        return 0;
    }
    // sorteer de getallen:
    qsort(a, aantal_a, sizeof(int), compare_ints);
    // De unieke getallen worden naar voren gekopieerd.
    int uniek_i = 1;
    for (int i = 1; i < aantal_a; i++)
    {
        // check of a[i] uniek is t.o.v. zijn voorganger
        if (a[i] != a[uniek_i-1])
        {
            a[uniek_i++] = a[i];
        }
    }
    return uniek_i;
}

int main(void)
{
    int rij1[] = { 23, 3, 56, 3, 23, 5, 12, 6, 3, 18, 7, 10, 56, 11 };
    int rij2[] = { 3, 3, 3, 3 };
    int aantal_uniek = uniek(rij1, sizeof rij1 / sizeof rij1[0]);
    for (int i = 0; i < aantal_uniek; i++)
    {
        printf("%d ", rij1[i]);
    }
    printf("\n");
    aantal_uniek = uniek(rij2, sizeof rij2 / sizeof rij2[0]);
    for (int i = 0; i < aantal_uniek; i++)
    {
        printf("%d ", rij2[i]);
    }
    printf("\n");

    // flauw maar moet wel werken
    aantal_uniek = uniek(rij2, 0);
    for (int i = 0; i < aantal_uniek; i++)
    {
        printf("%d ", rij2[i]);
    }
    printf("\n");

    return 0;
}
