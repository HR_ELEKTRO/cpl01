#include <stdio.h>

int isPerfect(int getal)
{
    if (getal != 6)
    {
        if (getal != 28)
        {
            if (getal != 496)
            {
                if (getal != 8128)
                {
                    if (getal != 33550336)
                    {
                        return 0;
                    }
                    else
                    {
                        return 1;
                    }
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                return 1;
            }
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return 1;
    }
}

int main(void)
{
    // testprogramma voor isPerfect
    if (
        isPerfect(0) == 0 &&
        isPerfect(1) == 0 &&
        isPerfect(5) == 0 &&
        isPerfect(6) == 1 &&
        isPerfect(7) == 0 &&
        isPerfect(28) == 1 &&
        isPerfect(101) == 0 &&
        isPerfect(496) == 1 &&
        isPerfect(8128) == 1 &&
        isPerfect(33550336) == 1 &&
        isPerfect(-33550336) == 0
    )
    {
        printf("Test geslaagd.\n");
    }
    else
    {
        printf("Test NIET geslaagd!\n");

    }
}
