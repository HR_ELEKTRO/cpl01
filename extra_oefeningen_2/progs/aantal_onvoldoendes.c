#include <stdio.h>

int aantal_onvoldoendes(double resultaten[], int n)
{
    int aantal = 0;
    for (int i = 0; i < n; i++)
    {
        if (resultaten[i] < 5.5)
        {
            aantal = aantal + 1;
        }
    }
    return aantal;
}

int main(void)
{
    double test_resultaten[] = {1.4, 6.4, 5.5, 9.4, 5.4, 7.8};

    if (
        aantal_onvoldoendes(test_resultaten, 6) == 2 &&
        aantal_onvoldoendes(test_resultaten, 2) == 1 &&
        aantal_onvoldoendes(test_resultaten, 1) == 1 &&
        aantal_onvoldoendes(test_resultaten, 0) == 0
    )
    {
        printf("Test geslaagd.\n");
    }
    else
    {
        printf("Test NIET geslaagd!\n");
    }
    return 0;
}
