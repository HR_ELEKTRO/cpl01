#include <stdio.h>
#include <string.h>
#include <ctype.h>

int is_spiegelwoord(char woord[])
{
    int begin = 0, eind = strlen(woord) - 1;
    while (begin < eind)
    {
        if (islower(woord[begin]) && woord[begin] == woord[eind])
        {
            ++begin;
            --eind;
        }
        else
        {
            return 0;
        }
    }
    return 1;
}

int main(void)
{
    if (
        is_spiegelwoord("meetsysteem")
        && is_spiegelwoord("soos")
        && !is_spiegelwoord("Lol")
        && !is_spiegelwoord("73neen37")
    )
    {
        printf("Test geslaagd.\n");
    }
    else
    {
        printf("Test NIET geslaagd!\n");
    }
    return 0;
}
