#include <stdio.h>

int main(void)
{
    for (int rij = 0; rij < 16; rij++)
    {
        for (int kolom = 0; kolom < 16; kolom++)
        {
            printf("%02X ", rij * 16 + kolom);
        }
        printf("\n");
    }
    return 0;
}
