#include <stdio.h>

void underscore(char s[])
{
    for (int i = 0; s[i] != '\0'; i = i + 1)
    {
        if (s[i] == ' ')
        {
            s[i] = '_';
        }
    }
}

int main(void)
{
    char string[] = "Wat is hier aan de hand?";
    underscore(string);
    printf("%s\n", string);

    return 0;
}
