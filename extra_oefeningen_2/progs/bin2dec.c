#include <stdio.h>

int bin2dec(int bin[])
{
    int dec = 0, power2 = 1;
    for (int bit = 0; bit < 8; bit++)
    {
        dec = dec + power2 * bin[7-bit];
        power2 = power2 * 2;
    }
    return dec;
}

int main(void)
{
    int b1[8] = {0, 0, 0, 0, 1, 0, 1, 1};
    int b2[8] = {1, 0, 1, 1, 1, 1, 0, 1};
    if (bin2dec(b1) == 11)
    {
        printf("Test 1 is geslaagd.\n");
    }
    else
    {
        printf("Test 1 is NIET geslaagd!\n");
    }
    if (bin2dec(b2) == 189)
    {
        printf("Test 2 is geslaagd.\n");
    }
    else
    {
        printf("Test 2 is NIET geslaagd!\n");
    }
    return 0;
}
